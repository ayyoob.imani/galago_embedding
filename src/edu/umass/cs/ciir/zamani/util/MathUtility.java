/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author zamani
 */
public class MathUtility {
    public static List <Double> normalize (List <Double> input){
        List <Double> result = new ArrayList <> ();
        double norm = norm(input);
        for (double d : input){
            result.add(d/norm);
        }
        return result;
    }

    public static double norm(List<Double> input) {
        double result = 0;
        for (double d : input){
            result += (d*d);
        }
        return Math.sqrt(result);
    }
    
    public static double dotProduct (List <Double> a, List <Double> b) throws Exception{
        if (a.size() != b.size())
            throw new Exception ("The dimension of two vectors are not the same! " + a.size() + " " + b.size());
        double result = 0;
        for (int i=0; i<a.size(); i++){
            result += (a.get(i) * b.get(i));
        }
        return result;
    }
    
    public static double modifiedDotProduct (List <Double> a, List <Double> b) throws Exception{
        if (a.size() != b.size())
            throw new Exception ("The dimension of two vectors are not the same! " + a.size() + " " + b.size());
        double result = 0;
        for (int i=0; i<a.size(); i++){
            result += (a.get(i) * b.get(i));
        }
        return (result+1.0)/2.0;
    }

    public static List<Double> vectorSubtract(List<Double> a, List<Double> b) throws Exception {
        if (a.size() != b.size())
            throw new Exception ("The dimension of two vectors are not the same!");
        List <Double> result = new ArrayList <> ();
        for (int i=0; i<a.size(); i++)
            result.add(a.get(i) - b.get(i));
        return result;
    }

    public static List<Double> vectorAdd(List<Double> a, List<Double> b) throws Exception {
        if (a.size() != b.size())
            throw new Exception ("The dimension of two vectors are not the same!");
        List <Double> result = new ArrayList <> ();
        for (int i=0; i<a.size(); i++)
            result.add(a.get(i) + b.get(i));
        return result;
    }
    
    public static double euclideanDistance (List <Double> a, List <Double> b) throws Exception{
        if (a.size() != b.size())
            throw new Exception ("The dimension of two vectors are not the same! " + a.size() + " " + b.size());
        double result = 0;
        for (int i=0; i<a.size(); i++){
            result += Math.pow(a.get(i) - b.get(i), 2);
        }
        return Math.sqrt(result);
    }
}
