/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;
import edu.umass.cs.ciir.zamani.prf.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.lemurproject.galago.core.parse.stem.Stemmer;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.prf.ExpansionModel;
import org.lemurproject.galago.core.retrieval.prf.RelevanceModel1;
import org.lemurproject.galago.core.retrieval.prf.WeightedTerm;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;


/**
 *
 * @author zamani
 */
public class QueryExpansionViaQueryVector implements ExpansionModel {
    private static final Logger logger = Logger.getLogger("EMB1");
    protected int defaultFbTerms;
    protected Retrieval retrieval;
    protected Set<String> exclusionTerms;
    protected Stemmer stemmer;
    protected WordEmbedding wordEmbedding;
    protected double defaultFbOrigWeight;
    List <Double> queryVec;
    Map <String, Double> prfDist;
    
    
    public QueryExpansionViaQueryVector (Retrieval r, WordEmbedding we, List <Double> queryVec, Parameters param) throws Exception {
        this.retrieval = r;
        defaultFbTerms = (int) Math.round(r.getGlobalParameters().get("fbTerm", param.get("fbterms", 30)));
        this.exclusionTerms = TermFactory.stopWordList();
        Parameters gblParms = r.getGlobalParameters();
        this.stemmer = GalagoUtility.getStemmer(gblParms, retrieval);
        wordEmbedding = we;
        defaultFbOrigWeight = r.getGlobalParameters().get("fbOrigWeight", param.get("fbOrigWeight", 0.5));
        this.queryVec = queryVec;
    }

    public QueryExpansionViaQueryVector (Retrieval r, WordEmbedding we, List <Double> queryVec, Parameters param, Map <String, Double> prfDist) throws Exception {
        this.retrieval = r;
        defaultFbTerms = (int) Math.round(r.getGlobalParameters().get("fbTerm", param.get("fbterms", 30)));
        this.exclusionTerms = TermFactory.stopWordList();
        Parameters gblParms = r.getGlobalParameters();
        this.stemmer = GalagoUtility.getStemmer(gblParms, retrieval);
        wordEmbedding = we;
        defaultFbOrigWeight = r.getGlobalParameters().get("fbOrigWeight", param.get("fbOrigWeight", 0.5));
        this.queryVec = queryVec;
        this.prfDist = prfDist;
    }
    
    
    public QueryExpansionViaQueryVector (Retrieval r, WordEmbedding we, List <Double> queryVec) throws Exception {
        this.retrieval = r;
        defaultFbTerms = (int) Math.round(r.getGlobalParameters().get("fbTerm", 30.0));
        this.exclusionTerms = TermFactory.stopWordList();
        Parameters gblParms = r.getGlobalParameters();
        this.stemmer = GalagoUtility.getStemmer(gblParms, retrieval);
        wordEmbedding = we;
        defaultFbOrigWeight = r.getGlobalParameters().get("fbOrigWeight", 0.5);
        this.queryVec = queryVec;
    }
    


    @Override
    public Node expand(Node root, Parameters queryParameters) throws Exception {
        Set <String> embeddingVocab = wordEmbedding.getVocab();
        int fbTerms = (int) Math.round(root.getNodeParameters().get("fbTerm", queryParameters.get("fbTerm", (double) defaultFbTerms)));

        if (fbTerms <= 0) {
            logger.info("fbTerms is invalid, no expansion possible. (<= 0)");
            return root;
        }
        List <WeightedTerm> expansionTerms = new ArrayList <> ();
        
        Node x = retrieval.transformQuery(root, queryParameters);
        Set <String> queryTerms = GalagoUtility.setmTermsIfNeeded(StructuredQuery.findQueryTerms(x), stemmer);
        
        
        System.err.println("# QUERY TERMS: " + queryTerms.size());
        for (String qTerm : queryTerms){
            System.err.println("######### " + qTerm);
            if (!embeddingVocab.contains(qTerm)){
                throw new Exception ("Vocabulary does not contain the query term: " + qTerm);
            }
        }
        if (queryTerms.size() == 0)
            return root;
        
        double sum = 0;
        Map <String, Double> pqv = new HashMap <> ();
        for (String term : embeddingVocab){
            // skip query terms and blacklist words
            if (queryTerms.contains(term) || exclusionTerms.contains(term) || !TermFactory.isValidTerm(term)) {
                continue; // on the blacklist
            }
            
            double sim = wordEmbedding.getSimilarity(queryVec, wordEmbedding.getVector(term));
            sum += sim;
            pqv.put(term, sim);
            expansionTerms.add(new RelevanceModel1.WeightedUnigram(term, sim));
        }
        
       /* double alpha = 1;
        double alaki = 0;
        for (String term : pqv.keySet()){
            double p = (1-alpha)*pqv.get(term)/sum;
            if (prfDist.containsKey(term))
                p += (alpha * prfDist.get(term));
            expansionTerms.add(new RelevanceModel1.WeightedUnigram(term, p));
            alaki += p;
        }
        System.err.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^ " + alaki);
*/        Collections.sort(expansionTerms);
        
        Node expNode = generateExpansionQuery(expansionTerms, fbTerms);
        queryParameters.set("defaultFbOrigWeight", defaultFbOrigWeight);
        return new LinearInterpolator().interpolate(root, expNode, queryParameters);
    }


    public Node expand(Node root, Parameters queryParameters, BufferedWriter outfile) throws Exception {
        return null;
    }


    public Node generateExpansionQuery(List<WeightedTerm> weightedTerms, int fbTerms) throws IOException, Exception {
        Node expNode = new Node("combine");
        double sum = 0;
        WordEmbedding we = WordEmbedding.getInstance(null, null, 0);
        for (int i = 0; i < Math.min(weightedTerms.size(), fbTerms); i++) {
            sum += weightedTerms.get(i).getWeight();
        }
        for (int i = 0; i < Math.min(weightedTerms.size(), fbTerms); i++) {
            Node expChild = new Node("text", weightedTerms.get(i).getTerm());
            expNode.addChild(expChild);
            expNode.getNodeParameters().set("" + i, weightedTerms.get(i).getWeight()/sum);
            System.err.println("///////////////// " + weightedTerms.get(i).getTerm() + " " + weightedTerms.get(i).getWeight()/sum + " " + we.getVocab().contains(weightedTerms.get(i).getTerm()));
        }
        return expNode;
    }
}