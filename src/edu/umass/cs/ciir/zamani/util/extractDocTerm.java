package edu.umass.cs.ciir.zamani.util;

import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.GroupRetrieval;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.util.WordLists;

import javax.print.Doc;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.*;


public class extractDocTerm {
    public static void main(String[] args) throws Exception{
        String indexPath = "/home/reyhane/IR/AP/galagoIndex";
        String documentNameFile = "/home/reyhane/IR/AP/DOCNO";
        new extractDocTerm().extractTerms(indexPath , documentNameFile);
    }

    public void extractTerms(String indexPath , String docNameFile) throws Exception{
        Retrieval retrieval = RetrievalFactory.instance(indexPath);
        Set<String> exclusionTerms = WordLists.getWordList(retrieval.getGlobalParameters().get("rmstopwords", "rmstop"));
        List<String> docNames = getDocNo(docNameFile);
        List<String> documentsTerms = new ArrayList<>();
        Document doc;

        Document.DocumentComponents corpusParams = new Document.DocumentComponents(true, false, true);

        for (String docNo : docNames) {
            doc = retrieval.getDocument(docNo, corpusParams);
            List<String> docterms = new ArrayList<>();
            try {
                docterms = doc.terms;
            }
            catch (NullPointerException e){
                System.err.println(docNo);
            }

            for (String term : docterms) {
                if (exclusionTerms.contains(term)) {
                    continue; // on the blacklist
                }
                if (!documentsTerms.contains(term)) {
                    documentsTerms.add(term);
                }
             }
        }
        writeDocTerms(documentsTerms);

    }

    public void writeDocTerms(List<String>documentsTerm) throws IOException{
        BufferedWriter writer = new BufferedWriter(new FileWriter("/home/reyhane/IR/AP/DocTerms.txt"));
        for(String term : documentsTerm) {
            writer.write(term + "\n");
        }
        writer.close();
    }
    public List<String> getDocNo(String filename){
        List<String> DocNo = new ArrayList<>();
        try {
            File file = new File(filename);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                DocNo.add(line);
            }
            fileReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return DocNo;
    }
}
