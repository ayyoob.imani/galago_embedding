/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
import edu.umass.cs.ciir.zamani.util.MathUtility;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.query.Node;

/**
 *
 * @author zamani
 */
public class SigmoidPseudoQueryVector extends PseudoQueryVector{

    public SigmoidPseudoQueryVector(Retrieval r, Node root, WordEmbedding we, Map<String, Double> prfDist) throws Exception {
        super(r, root, we, prfDist);
    }
    
    @Override
    protected List <Double> getGradient(List <Double> queryVec) throws Exception {
        List <Double> gradient = new ArrayList <> ();
        //Map <String, Double> allSim = we.getAllSimilarities(queryVec);
        
        List <Double> sumVector = new ArrayList <> ();
        for (int i=0; i<we.getDimension(); i++)
            sumVector.add(0.0);
        Map <String, Double> allDotProducts = new HashMap <> ();
        double sumAllDotProducts = 0.0;
        for (String term : we.getVocab()){
            List <Double> termVec = we.getVector(term);
            double val = MathUtility.dotProduct(queryVec, termVec);
            allDotProducts.put(term, val);
            sumAllDotProducts += val;
            for (int i=0; i<we.getDimension(); i++)
                sumVector.set(i, sumVector.get(i) + termVec.get(i));
        }
        
        int vocabSize = we.getVocab().size();
        double queryNorm = MathUtility.norm(queryVec);
        
        double norm = MathUtility.norm(queryVec);
        for (int i=0; i<we.getDimension(); i++){
            gradient.add(0.0);
        }
        
        for (String term : prfDist.keySet()){
            List <Double> termVec = we.getVector(term);
            if (termVec == null)
                continue;
            double totalDotProduct = MathUtility.dotProduct(queryVec, termVec);

            for (int i=0; i<we.getDimension(); i++){
                double dotProduct = MathUtility.dotProduct(termVec, MathUtility.normalize(queryVec));
                //double val = (we.getA() * 0.5 * ((dotProduct * norm - 2.0 * alphaVec.get(qterm) * totalDotProduct * MathUtility.norm(queryTermVectors.get(qterm)))/Math.pow(norm, 2)) * (1.0 - allSim.get(term)) * prfDist.get(term));
                double val = prfDist.get(term) 
                        * ( (termVec.get(i) + 2.0 * queryVec.get(i)) / (allDotProducts.get(term) + queryNorm)
                        - (sumVector.get(i) + 2.0 * vocabSize * queryVec.get(i)) / (sumAllDotProducts + vocabSize * queryNorm));
                gradient.set(i, gradient.get(i) + val);
            }
        }
        return gradient;
    }    
}
