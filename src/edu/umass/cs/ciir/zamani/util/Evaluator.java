/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

//import edu.umass.cs.ciir.zamani.prf.EmbeddingBasedRM;
import edu.umass.cs.ciir.zamani.util.MyBatchSearch;
//import edu.umass.cs.ciir.zamani.prf.EmbeddingModel1;
//import edu.umass.cs.ciir.zamani.prf.EmbeddingModel2;
//import edu.umass.cs.ciir.zamani.prf.EmbeddingRelevanceModel2;
//import edu.umass.cs.ciir.zamani.prf.RelevanceModel3;
import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
import edu.umass.cs.ciir.zamani.util.GalagoUtility;
import edu.umass.cs.ciir.zamani.util.QueryParser;
import edu.umass.cs.ciir.zamani.util.ResultWriter;
import edu.umass.cs.ciir.zamani.util.TermFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.prf.ExpansionModel;
import org.lemurproject.galago.core.retrieval.prf.RelevanceModel1;
import org.lemurproject.galago.core.retrieval.prf.RelevanceModel3;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import static org.lemurproject.galago.core.tools.apps.BatchSearch.logger;
import org.lemurproject.galago.utility.Parameters;

/**
 *
 * @author zamani
 */
public class Evaluator {
    public static void main(String[] args) throws Exception{
        System.out.println("hi sag");
    //    GeneralUtil.TrecTextToPlainText("/home/zamani/Desktop/datasets/raw/robust04", "/mnt/scratch/plain/robust04.txt");
        String dataset = "AP";
        String indexPath = "/home/vandermonde/Documents/galago/index";
        String outputFileName = "/home/vandermonde/Documents/galago/results/outputEQE2_";
        String queryFileName = "/home/vandermonde/Documents/galago/queries.xml";
        TermFactory.create(indexPath);
        new Evaluator().retrieve(indexPath, queryFileName, outputFileName);
    }

    public void retrieve(String indexPath, String queryFileName, String outputFileName) throws Exception {
        List<ScoredDocument> results;
        int requested = 1000;
        boolean append = false;
        boolean queryExpansion = true;
//        ResultWriter resultWriter = new ResultWriter(outputFileName, append);
        
        // open index
        Retrieval retrieval = RetrievalFactory.instance(indexPath);
        
        // read word embedding vectors
        int dim = 200;
        WordEmbedding we = WordEmbedding.getInstance("/home/vandermonde/Documents/lemur/lemur_word_order_ir/positive_terms/data/embedding/glove.6B.200d.txt", "/home/vandermonde/Documents/galago/similarities200d_.allsim-final", dim);

        // get queries
        List<Parameters> queries = MyBatchSearch.getQueries(retrieval, we, QueryParser.trecToGalagoFormat(queryFileName, retrieval, true));//.subList(0, 50);



        int []nterms = {30, 50, 70 ,100};//{20, 40, 60, 80, 100};//{10,20,30,40,50,60};//,70,80,90,100};
        int []nterms2 = {10};//, 20, 30, 40, 50};
        double []alphas = {0.3,  0.5,  0.7,  0.9};//{0.3, 0.4, 0.5, 0.6, 0.7};
        double []cs = { 0.8};
        double []as = {0.8};
        double []alphas2 = {0.3};//{0, 0.2, 0.4, 0.6, 0.8};//{0.3};//{0.1, 0.3, 0.4};//, 0.5, 0.6, 0.7};
        for (int nterm : nterms){
            for (double alpha : alphas){
                for (double c : cs){
                    for (double a : as){

                    ResultWriter resultWriter = new ResultWriter(outputFileName+"-"+String.valueOf(nterm) + "-" + String.valueOf(alpha) + "-"+String.valueOf(c) + "-" + String.valueOf(a)+".txt", append);
                        BufferedWriter writer = new BufferedWriter(new FileWriter("EQE1.exp.file"+"-"+String.valueOf(nterm) + "-" + String.valueOf(alpha) + "-"+String.valueOf(c) + "-" + String.valueOf(a)+".txt"));
                    // for each query, run it, get the results, print in TREC format
                        for (Parameters query : queries) {
                            String queryNumber = query.getString("number");
                            String queryText = query.getString("text");
                            queryText = queryText.toLowerCase(); // option to fold query cases -- note that some parameters may require upper case

                            logger.info("Processing query #" + queryNumber + ": " + queryText);


                            //query.setBackoff(parameters);
                            query.set("requested", requested);
                            query.set("part", "postings");
                            query.set("defaultTextPart", "postings");

                            // parse and transform query into runnable form
                            Node root = StructuredQuery.parse(queryText);
                            Node transformed = retrieval.transformQuery(root, query);

                            // Query Expansion
                            if (queryExpansion){
                                Parameters EQE1param = Parameters.create();
                                EQE1param.set("fbterms", 100);
                                EQE1param.set("fbOrigWeight", 0.9);

                                Parameters EQE2param = Parameters.create();
                                EQE2param.set("fbterms", nterm);
                                EQE2param.set("fbOrigWeight", alpha);

                                Parameters Topicparam = Parameters.create();
                                Topicparam.set("fbDocs" , 10);
                                Topicparam.set("fbterms" , 5);
                                Topicparam.set("fbOrigWeight" , 0.5);


//                                Map <String, Double> prfDist = new RM3Distribution(retrieval, param2).getPRFDist(root.clone(), query.clone());
//                                QueryVector queryVector = new PseudoQueryOptimizer(dim, prfDist, we);
//                                ExpansionModel qe = new QueryExpansionViaQueryVector(retrieval, we, queryVector.getQueryVector(), param);

//                                ExpansionModel qe = new EQE1(retrieval, we, EQE1param);
                                we.setA(a);
                                we.setC(c);
                                EQE2 qe = new EQE2(retrieval, we, EQE2param);
//                                ExpansionModel qe = new EmbeddingBasedRM(retrieval, we);
//                                ExpansionModel qe = new RelevanceModel1(retrieval);
//                                ExpansionModel qe = new QE_With_TopicWords(retrieval , Topicparam);

//                                logger.info("not expanded query:\n" + transformed.toPrettyString());
                                Node expandedQuery = qe.expand(root.clone(), query.clone(), writer);

//                                System.out.println(expandedQuery.getNodeParameters().toString());

                                transformed = retrieval.transformQuery(expandedQuery, query);

                            }
                            logger.info("Transformed Query:\n" + transformed.toPrettyString());

                            // run query
                            results = retrieval.executeQuery(transformed, query).scoredDocuments;

                            // print results
                            resultWriter.write(queryNumber, results);
                        }
                        writer.close();
                        resultWriter.close();
                        we.writeAllSims("/home/vandermonde/Documents/galago/similarities200d_.allsim-final");
                        we.setAllSimilarityChanged(false);
                    }
                }
            }
        }
    }
}


//TODO
