/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import org.lemurproject.galago.core.index.IndexPartReader;
import org.lemurproject.galago.core.index.disk.DiskIndex;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.util.WordLists;

/**
 *
 * @author zamani
 */
public class TermFactory {
    private static final Logger logger = Logger.getLogger("TermFactory");
    static Set <String> notStemmedVocab = null;
    static Set <String> stopwords = null;
    static boolean all = false;
    static long uniqueTermCount;
    static long collectionLength;

    public static void create(String indexPath) throws IOException, Exception {
        create(indexPath, false);
    }
    
    public static void create(String indexPath, boolean stemming) throws IOException, Exception {
        long zero = 0;
        IndexPartReader reader;
        if (stemming){
            notStemmedVocab = GalagoUtility.getAllVocabularyTerms(indexPath + "/postings.krovetz");
            reader = DiskIndex.openIndexPart(indexPath + "/postings.krovetz");
        }
        else{
            notStemmedVocab = GalagoUtility.getAllVocabularyTerms(indexPath + "/postings");
            reader = DiskIndex.openIndexPart(indexPath + "/postings");
        }
        uniqueTermCount = reader.getManifest().get("statistics/collectionLength", zero);
        collectionLength = reader.getManifest().get("statistics/vocabCount", zero);
        stopwords = WordLists.getWordList(RetrievalFactory.instance(indexPath).getGlobalParameters().get("rmstopwords", "rmstop"));
        if (notStemmedVocab.size() == 0 || stopwords.size() == 0){
            logger.info("There is not vocab or no stopwords!");
        }
        all = false;
    }
    
    public static void create(String[] indexPaths, boolean stemming) throws IOException, Exception {
        long zero = 0;
        IndexPartReader reader;
        notStemmedVocab = new HashSet<>();
        for (String indexPath : indexPaths){
            if (stemming){
                notStemmedVocab.addAll(GalagoUtility.getAllVocabularyTerms(indexPath + "/postings.krovetz"));
                reader = DiskIndex.openIndexPart(indexPath + "/postings.krovetz");
            }
            else{
                notStemmedVocab.addAll(GalagoUtility.getAllVocabularyTerms(indexPath + "/postings"));
                reader = DiskIndex.openIndexPart(indexPath + "/postings");
            }
            uniqueTermCount = reader.getManifest().get("statistics/collectionLength", zero);
            collectionLength = reader.getManifest().get("statistics/vocabCount", zero);
            stopwords = WordLists.getWordList(RetrievalFactory.instance(indexPath).getGlobalParameters().get("rmstopwords", "rmstop"));
            if (notStemmedVocab.size() == 0 || stopwords.size() == 0){
                logger.info("There is not vocab or no stopwords!");
            }
            all = false;
        }
    }
    
    public static void create() throws IOException, Exception {
        all = true;
    }
    
    public static Set <String> stopWordList(){
        return stopwords;
    }
    
    public static Set <String> notStemmedVocabList(){
        return notStemmedVocab;
    }
    
    public static boolean isStopword (String term){
        if (all)
            return false;
        return stopwords.contains(term);
    }
    
    public static boolean isValidTerm (String term){
        if (all)
            return true;
        return notStemmedVocab.contains(term);
    }
    
    public static long collectionLength(){
        return collectionLength;
    }
    
    public static long uniqueTermCount(){
        return uniqueTermCount;
    }
    
}
