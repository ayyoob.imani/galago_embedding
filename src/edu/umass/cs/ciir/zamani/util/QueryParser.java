/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import com.sun.org.apache.xml.internal.utils.WrongParserException;

import java.io.*;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 *
 * @author zamani
 */
public class QueryParser {
    public static void main(String[] args) throws Exception{
        List <Parameters> queries = trecToGalagoFormat(args[0]);
        for (Parameters q : queries){
            String queryText = q.getString("text");
            String queryNumber = q.getString("number");
            System.err.println(queryNumber + " " + queryText);
            System.err.println("---------------");
        }
    }

    public static List <Parameters> trecToGalagoFormat (String fileName) throws IOException, FileNotFoundException, ParserConfigurationException, SAXException {
        return getGalagoFormatQueries(readTrecQueries(fileName));
    }

    public static List <Parameters> trecToGalagoFormat (String fileName, Retrieval r, boolean rmvStopwords) throws IOException, FileNotFoundException, ParserConfigurationException, SAXException, Exception {
        if (rmvStopwords)
            return getGalagoFormatQueries(removeStopwords(readTrecQueries(fileName), r));
        else
            return getGalagoFormatQueries(readTrecQueries(fileName));
    }

    public static List <Parameters> readGalagoFormat (String fileName, Retrieval r, boolean rmvStopwords) throws IOException, FileNotFoundException, ParserConfigurationException, SAXException, Exception {
        if (rmvStopwords)
            return getGalagoFormatQueries(removeStopwords(readGalagoQueries(fileName), r));
        else
            return getGalagoFormatQueries(readGalagoQueries(fileName));
    }

    public static List <Pair <String, String>> readGalagoQueries (String fileName) throws FileNotFoundException, IOException{
        List <Pair <String, String>> queries = new ArrayList <> ();

        String wholeFile = "";
        String line;
        FileReader fileReader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while((line = bufferedReader.readLine()) != null) {
            wholeFile += line;
        }

        bufferedReader.close();

        JsonReader reader = Json.createReader(new StringReader(wholeFile));

        JsonObject fileData = reader.readObject();
        reader.close();

        JsonArray queriesArray = fileData.getJsonArray("queries");
        for(JsonObject queryData: queriesArray.getValuesAs(JsonObject.class)){
            String queryNom = queryData.getString("number");
            String text = queryData.getString("text");
            queries.add(new Pair(queryNom, text));
        }

        System.out.println(queries.size() + " queries have been read from file.");
        return queries;
    }

    public static List <Pair <String, String>> readTrecQueries (String fileName) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException{
        List <Pair <String, String>> queries = new ArrayList <> ();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        String xmlRecords = new Scanner(new File(fileName)).useDelimiter("\\Z").next();
        xmlRecords = "<data>" + xmlRecords + "</data>";
        xmlRecords = xmlRecords.replaceAll("&", "&amp;");
        xmlRecords = xmlRecords.replaceAll("\n", " ");
        is.setCharacterStream(new StringReader(xmlRecords));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("DOC");

        // iterate the employees
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            NodeList qNum = element.getElementsByTagName("DOCNO");
            Element line = (Element) qNum.item(0);
            String queryNumber = getCharacterDataFromElement(line).trim();

            NodeList title = element.getElementsByTagName("TEXT");

            line = (Element) title.item(0);
            String queryText = getCharacterDataFromElement(line).trim();
            while (queryText.endsWith(".")){ // because of Galago bug in StructuredQuery parsing
                queryText = queryText.substring(0, queryText.length()-1);
            }
            //System.err.println("~~~~ " + queryNumber + ": " + queryText);
            queries.add(new Pair(queryNumber, queryText));
        }
        System.out.println(queries.size() + " queries have been read from file.");
        return queries;
    }

    public static List <Parameters> getGalagoFormatQueries (List <Pair <String, String>> inputQueries) throws IOException{
        List <Parameters> queries = new ArrayList <> ();
        for (Pair q : inputQueries){
            queries.add(Parameters.parseString(String.format("{\"number\":\"%s\", \"text\":\"%s\"}", q.getFirst(), q.getSecond())));
        }
        return queries;
    }

    public static String getCharacterDataFromElement(Element e) throws WrongParserException {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        throw new WrongParserException("WRONG TREC FORMAT QUERY!");
    }

    public static List <Pair <String, String>> removeStopwords (List <Pair <String, String>> inputQueries, Retrieval r) throws Exception{
        List <Pair <String, String>> result = new ArrayList <> ();
        int k = 0;
        for (Pair <String, String> query : inputQueries){
            //System.err.println("@@@@@@ " + query.getFirst() + ": " + query.getSecond().toLowerCase());
            org.lemurproject.galago.core.retrieval.query.Node root = StructuredQuery.parse(query.getSecond().toLowerCase() + " ");
            //System.err.println(root.toSimplePrettyString());
            try{
//                System.err.println(query.getFirst());
                root = r.transformQuery(root, Parameters.create());
            } catch (Exception ex){
                ex.printStackTrace();
//                continue;
            }
            Set <String> qTerms = GalagoUtility.setmTermsIfNeeded(StructuredQuery.findQueryTerms(root), null);
            String qText = "";
            //System.err.println(qTerms.size());
            for (String qTerm : qTerms){
                if (!TermFactory.isStopword(qTerm)){
                    qText += (qTerm + " ");
//                    System.err.println("!!! " + query.getFirst() + " " + query.getSecond() + ": " + qTerm);
                }
                //System.err.println("!!! " + query.getFirst()+ ": " + qTerm);
            }
            qText = qText.trim();
            result.add(new Pair <String, String> (query.getFirst(), qText));
        }
        return result;
    }
}
