/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.query.Node;

/**
 *
 * @author zamani
 */
public class GridSearchPseudoQueryVector extends PseudoQueryVector{

    public GridSearchPseudoQueryVector(Retrieval r, Node root, WordEmbedding we, Map<String, Double> prfDist) throws Exception {
        super(r, root, we, prfDist);
    }
    
    @Override
    public List <Double> getQueryVector () throws Exception{
        int qlen = queryTermVectors.size();
        double maxLikelihood = Double.NEGATIVE_INFINITY;
        Map <String, Double> result = null;
        if (qlen == 1){
            for (String term : queryTermVectors.keySet())
                return queryTermVectors.get(term);
        }
        double[] domain = {0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75};
        int n = (int)Math.pow(domain.length, qlen-1);
        for (int i=0; i<n; i++){
            List <Double> alpha = getPermutations(qlen, domain, 1.0, 0.0, i);
            //List <Double> alpha = new ArrayList <> ();
            //for (int i=0; i<qlen; i++)
            //    alpha.add(1.0/qlen);
            if (alpha == null)
                continue;
            Map <String, Double> alphaMap = new HashMap <> ();
            int k = 0;
            for (String term : queryTermVectors.keySet()){
                alphaMap.put(term, alpha.get(k));
                k++;
            }
            double likelihood = loglikelihood(getQueryVector(alphaMap));
            if (likelihood > maxLikelihood){
                maxLikelihood = likelihood;
                result = new HashMap <> ();
                result.putAll(alphaMap);
                System.err.println("likelihood: " + likelihood);
                for (double d : alpha)
                    System.err.print(d + " ");
                System.err.println();
            }
        }
        return getQueryVector(result);
    }
    
    List <Double> getPermutations (int qlen, double[] domain, double sum, double min, int i){ // domain = 0 0.1 ... 1
        List <Double> result = new ArrayList <> ();
        int temp = i;
        double tempSum = 0.0;
        for (int j=0; j<qlen-1; j++){
            tempSum += domain[temp%domain.length];
            result.add(domain[temp%domain.length]);
            temp /= domain.length;
        }
        result.add(sum - tempSum);
        if (sum - tempSum < min)
            return null;
        return result;
    }
}
