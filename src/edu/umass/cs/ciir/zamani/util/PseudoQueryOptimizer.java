///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package edu.umass.cs.ciir.zamani.util;
//
//import cc.mallet.optimize.LimitedMemoryBFGS;
//import cc.mallet.optimize.Optimizable;
//import cc.mallet.optimize.Optimizable.ByGradientValue;
//import cc.mallet.optimize.OptimizationException;
//import cc.mallet.optimize.Optimizer;
//import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
//import edu.umass.cs.ciir.zamani.util.MathUtility;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author zamani
// */
//public class PseudoQueryOptimizer implements QueryVector{
//        int dim;
//        Map <String, Double> prfDist;
//        WordEmbedding we;
//
//    public PseudoQueryOptimizer(int dim, Map <String, Double> prfDist, WordEmbedding we) {
//        this.dim = dim;
//        this.prfDist = prfDist;
//        this.we = we;
//    }
//
//    @Override
//    public List<Double> getQueryVector() throws Exception {
//        List <Double> initQVec = new ArrayList <> ();
//
//        for (int i=0; i<we.getDimension(); i++)
//            initQVec.add(0.0);
//
//        for (String term : prfDist.keySet()){
//            List <Double> vec = we.getVector(term);
//            if (vec == null)
//                continue;
//            for (int i=0; i<we.getDimension(); i++){
//                initQVec.set(i, initQVec.get(i) + (prfDist.get(term)) * vec.get(i));
//            }
//        }
//        initQVec = MathUtility.normalize(initQVec);
//        //if (true)
//            //return initQVec;
//        PseudoQueryVectorOptimizer optimizable = new PseudoQueryVectorOptimizer(initQVec);
//        Optimizer optimizer = new LimitedMemoryBFGS(optimizable);
//
//        boolean converged = false;
//
//        try {
//            converged = optimizer.optimize();
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//            // This exception may be thrown if L-BFGS
//            //  cannot step in the current direction.
//            // This condition does not necessarily mean that
//            //  the optimizer has failed, but it doesn't want
//            //  to claim to have succeeded...
//        } catch (OptimizationException e){
//            e.printStackTrace();
//        }
//
//        return optimizable.getVector();
//    }
//
//
//    public class PseudoQueryVectorOptimizer implements Optimizable.ByGradientValue {
//
//        // Optimizables encapsulate all state variables,
//        //  so a single Optimizer object can be used to optimize
//        //  several functions.
//
//        double[] parameters;
//        //int dim;
//      //  Map <String, Double> prfDist;
//       // WordEmbedding we;
//
//        public PseudoQueryVectorOptimizer(List <Double> initialVector) {
//         //   this.dim = dim;
//            parameters = new double [dim];
//            for (int i=0; i<dim; i++){
//                parameters[i] = initialVector.get(i);
//            }
//          //  this.prfDist = prfDist;
//          //  this.we = we;
//        }
//
//        public double getValue() {
//            double res = 0;
//            List qvec = getVector();
//            double norm = MathUtility.norm(qvec);
//            List <Double> normalizedQvec = MathUtility.normalize(qvec);
//            for (String term : prfDist.keySet()){
//                List <Double> vec = we.getVector(term);
//                if (vec == null)
//                    continue;
//                try {
//                    res += (prfDist.get(term) * Math.log(we.getSimilarity(vec, normalizedQvec)));
//                } catch (Exception ex) {
//                    System.err.println("WHYYYYYYYYYYY?");
//                }
//            }
//            return res;
//        }
//
//        public void getValueGradient(double[] gradient) {
//            List <Double> qvec = getVector();
//            double norm = MathUtility.norm(qvec);
//            for (int i=0; i<dim; i++)
//                gradient[i] = 0;
//            List <Double> normalizedQvec = MathUtility.normalize(qvec);
//            for (String term : prfDist.keySet()){
//                List <Double> vec = we.getVector(term);
//                if (vec == null)
//                        continue;
//                double dot = 0;
//                double sim = 0;
//                try {
//                    dot = MathUtility.dotProduct(qvec, vec);
//                    sim = (we.getSimilarity(vec, normalizedQvec));
//                } catch (Exception ex) {
//                    Logger.getLogger(PseudoQueryOptimizer.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                double a = we.getA()/2;
//                for (int i=0; i<dim; i++){
//                    //gradient[i] += (prfDist.get(term) * (1.0 + 0.5 * we.getA() * vec.get(i) * sim));
//                    gradient[i] += (prfDist.get(term) * a * ((vec.get(i) / norm) - (2.0 * qvec.get(i) * dot / (norm * norm * norm))) * (1.0 - sim));
//                }
//            }
//        }
//
//        // The following get/set methods satisfy the Optimizable interface
//
//        public int getNumParameters() { return dim; }
//        public double getParameter(int i) { return parameters[i]; }
//        public void getParameters(double[] buffer) {
//            for (int i=0; i<dim; i++)
//                buffer[i] = parameters[i];
//        }
//
//        public List <Double> getVector (){
//            List <Double> res = new ArrayList <>();
//            for (int i=0; i<dim; i++)
//                res.add(parameters[i]);
//            return res;
//        }
//
//        public void setParameter(int i, double r) {
//            parameters[i] = r;
//        }
//        public void setParameters(double[] newParameters) {
//            for (int i=0; i<dim; i++)
//                parameters[i] = newParameters[i];
//        }
//    }
//}
