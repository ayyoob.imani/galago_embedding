package edu.umass.cs.ciir.zamani.util;

import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.prf.ExpansionModel;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;


import javax.print.Doc;
import java.io.IOException;
import java.util.*;
import java.io.*;
import java.util.logging.Logger;

public class QE_With_TopicWords implements ExpansionModel {

    private static final Logger logger = Logger.getLogger("QE with Topic modeling");
    private int fbTerms;
    private double fbOrigWeight;
    private int fbDocs;
    private Retrieval retrieval;

    public QE_With_TopicWords(Retrieval r , Parameters param){

        this.retrieval = r;
        fbDocs = param.get("fbDocs" , 10);
        fbTerms = param.get("fbterms" , 5);
        fbOrigWeight = param.get("fbOrigWeight" , 0.5);
    }

    @Override
    public Node expand(Node root , Parameters queryParameters) throws Exception {

        if (fbDocs <= 0 || fbTerms <= 0) {
            logger.info("fbDocs, or fbTerms is invalid, no expansion possible. (<= 0)");
            return root;
        }

        // set parameters to retrieve the results
        Parameters fbParams = Parameters.create();
        fbParams.set("requested" , fbDocs);
        fbParams.set("passageQuery", false);
        fbParams.set("extentQuery", false);
        fbParams.setBackoff(queryParameters);// add text and number of the query

        // collect initial results
        Node transformed = retrieval.transformQuery(root.clone(), fbParams);
        Results results = retrieval.executeQuery(transformed, fbParams);
        List<ScoredDocument> feedbackDocs = results.scoredDocuments;

        // read topics
        Map<Integer, Topic> topics = readTopics("/home/reyhane/IR/AP/TopicModeling/topics_LDA.txt");

        // read Document distribution
        Map<String, Map<Integer,Double>> DocTopicDist = DocumentTopicsDistribution("/home/reyhane/IR/AP/TopicModeling/AP_Topics.txt");


        Node expand = new Node();
        return expand;
    }


    public Node expand(Node root, Parameters queryParameters, BufferedWriter outfile) throws Exception {
        return null;
    }

    public Map<String,Map<Integer,Double>> DocumentTopicsDistribution(String fileName) throws IOException{
        Map<String,Map<Integer,Double>> DocDist = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));

        String str;
        while((str = reader.readLine()) != null){
            String[] split_str = str.split("\\s+");
            Map<Integer,Double> distribution = new HashMap<>();

            for (int i = 1 ; i < split_str.length ; i++){
                distribution.put(i-1 , Double.parseDouble(split_str[i]));
            }
            DocDist.put(split_str[0], sortByValue(distribution));
            System.out.println(sortByValue(distribution).keySet());
        }
        return DocDist;
    }

    public Map<Integer,Topic> readTopics(String fileName) throws IOException{
        Map<Integer,Topic> topics = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(new File(fileName)));

        String str;
        while((str = reader.readLine()) != null){

            String[] split_str = str.split("\\s+");
            Topic topic = new Topic(Integer.parseInt(split_str[0]));

            for (int term = 1 ; term < split_str.length ; term++) {
                topic.addTopic(split_str[term], Integer.parseInt(split_str[term + 1].substring(1, split_str[term + 1].length() - 1)));
                term++;
            }
            topic.sortByValue();
            topic.calcTotalWeight();
            topics.put(Integer.parseInt(split_str[0]), topic);
        }

        return topics;
    }

    private static Map<Integer, Double> sortByValue(Map<Integer, Double> unsortMap) {

        // 1. Convert Map to List of Map
        List<Map.Entry<Integer, Double>> list = new LinkedList<Map.Entry<Integer, Double>>(unsortMap.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<Integer, Double>>() {
            public int compare(Map.Entry<Integer, Double> o1,
                               Map.Entry<Integer, Double> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<Integer, Double> sortedMap = new LinkedHashMap<Integer, Double>();
        for (Map.Entry<Integer, Double> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }


}
