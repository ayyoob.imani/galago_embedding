/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import org.lemurproject.galago.core.index.IndexPartReader;
import org.lemurproject.galago.core.index.KeyIterator;
import org.lemurproject.galago.core.index.disk.DiskIndex;
import org.lemurproject.galago.core.parse.stem.Stemmer;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.utility.Parameters;

/**
 *
 * @author zamani
 */
public class GalagoUtility {
    private static final Logger logger = Logger.getLogger("GalagoUtil");
    
    public static Stemmer getStemmer(Parameters p, Retrieval ret) {
        Stemmer stemmer;
        if (ret.getGlobalParameters().isString("rmStemmer")) {
            String rmstemmer = ret.getGlobalParameters().getString("rmStemmer");
            try {
                stemmer = (Stemmer) Class.forName(rmstemmer).getConstructor().newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            stemmer = null;
        }
        return stemmer;
    }
    
    
    public static Set <String> getAllVocabularyTerms (String fileName) throws IOException{
        Set <String> result = new HashSet<> ();
        IndexPartReader reader = DiskIndex.openIndexPart(fileName);
        if (reader.getManifest().get("emptyIndexFile", false)) {
            // do something!
        }

        KeyIterator iterator = reader.getIterator();
        while (!iterator.isDone()) {
          result.add(iterator.getKeyString());
          iterator.nextKey();
        }
        reader.close();
        logger.info(result.size() + " vocabulary terms are read from " + fileName);
        return result;
    }
    
    public static Set <String> setmTermsIfNeeded (Set <String> terms, Stemmer stemmer){
        if (stemmer == null)
            return terms;

        Set<String> stems = new HashSet<String>(terms.size());
        for (String t : terms) {
            String s = stemmer.stem(t);
            // stemmers should ensure that terms do not stem to nothing.
            stems.add(s);
        }
        return stems;
    }
}
