package edu.umass.cs.ciir.zamani.util;

import java.util.*;

public class Topic {

    public int topicNumber;
    public int totalWeight;
    public Map<String,Integer> topicWords;

    public Topic(int tNum){
        topicWords = new HashMap<>();
        topicNumber = tNum;
    }

    public int getWeight(String name) {
        if (topicWords.containsKey(name))
            return topicWords.get(name);
        else
            return 0;
    }

    public void addTopic(String name , int weight){
        topicWords.put(name , weight);
    }

    public void calcTotalWeight(){
        int w = 0;
        for (String term : topicWords.keySet()){
            w = w + topicWords.get(term);
        }
        totalWeight = w ;
    }

    public void sortByValue() {

        // 1. Convert Map to List of Map
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(topicWords.entrySet());

        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        topicWords = sortedMap;
    }

//    public Map<String, Double> getTop(int num){
//
//    }




}
