/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import org.lemurproject.galago.core.retrieval.prf.RelevanceModel1;
import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
import edu.umass.cs.ciir.zamani.util.GalagoUtility;
import edu.umass.cs.ciir.zamani.util.TermFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.parse.stem.Stemmer;
import org.lemurproject.galago.core.retrieval.GroupRetrieval;
import org.lemurproject.galago.core.retrieval.Results;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.prf.WeightedTerm;
import org.lemurproject.galago.core.retrieval.query.AnnotatedNode;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.core.util.WordLists;
import org.lemurproject.galago.utility.MathUtils;
import org.lemurproject.galago.utility.Parameters;

/**
 *
 * @author zamani
 */
public class RM3Distribution {

  private static final Logger logger = Logger.getLogger("RM1");
  protected final Retrieval retrieval;
  protected int defaultFbDocs;
  protected int defaultFbTerms;
  protected Set<String> exclusionTerms;
  protected Set<String> inclusionTerms;
  protected Stemmer stemmer;
  protected double defaultFbOrigWeight;

  Map<String, Map<ScoredDocument, Integer>> queryCount;

  public RM3Distribution(Retrieval r, Parameters param) throws Exception {
    this.retrieval = r;
    defaultFbDocs = (int) Math.round(r.getGlobalParameters().get("fbDocs", 10.0));
    defaultFbTerms = (int) Math.round(r.getGlobalParameters().get("fbTerm", param.get("fbterms", 100.0)));
    defaultFbOrigWeight = r.getGlobalParameters().get("fbOrigWeight", param.get("fbOrigWeight", 0.3));

    exclusionTerms = TermFactory.stopWordList();
    inclusionTerms = null;
    Parameters gblParms = r.getGlobalParameters();

    if (gblParms.isString("rmwhitelist")){
        inclusionTerms = WordLists.getWordList(r.getGlobalParameters().getString("rmwhitelist"));
    }
    this.stemmer = GalagoUtility.getStemmer(gblParms, retrieval);
    queryCount = new HashMap <> ();
  }

  public Map <String, Double> getPRFDist(Node root, Parameters queryParameters) throws Exception {

    int fbDocs = (int) Math.round(root.getNodeParameters().get("fbDocs", queryParameters.get("fbDocs", (double) defaultFbDocs)));
    int fbTerms = (int) Math.round(root.getNodeParameters().get("fbTerm", queryParameters.get("fbTerm", (double) defaultFbTerms)));
    double fbOrigWeight = root.getNodeParameters().get("fbOrigWeight", queryParameters.get("fbOrigWeight", (double) defaultFbOrigWeight));

    if (fbDocs <= 0 || fbTerms <= 0) {
      logger.info("fbDocs, or fbTerms is invalid, no expansion possible. (<= 0)");
      return null;
    }

    // transform query to ensure it will run
    Parameters fbParams = Parameters.create();
    fbParams.set("requested", fbDocs);
    // first pass is asserted to be document level
    fbParams.set("passageQuery", false);
    fbParams.set("extentQuery", false);
    fbParams.setBackoff(queryParameters);

    Node transformed = retrieval.transformQuery(root.clone(), fbParams);

    // get some initial results
    List<ScoredDocument> initialResults = collectInitialResults(transformed, fbParams);
    if (initialResults.isEmpty()) {
        System.err.println("^^^^^^^^^^^^^^^^^^^6");
        return null;
    }
    // extract grams from results
    Set<String> queryTerms = getTerms(stemmer, StructuredQuery.findQueryTerms(transformed));
    Set<String> exclusions = (fbParams.isString("rmstopwords")) ? WordLists.getWordList(fbParams.getString("rmstopwords")) : exclusionTerms;
    Set<String> inclusions = null;
    if (fbParams.isString("rmwhitelist")){
        inclusions = WordLists.getWordList(fbParams.getString("rmwhitelist"));
    } else {
        inclusions = inclusionTerms;
    }

    List<WeightedTerm> weightedTerms = extractGrams(retrieval, initialResults, stemmer, fbParams, queryTerms, exclusions, inclusions);

    double sum = 0;
    for (int i=0; i< Math.min(fbTerms, weightedTerms.size()); i++){
        sum += weightedTerms.get(i).getWeight();
    }

    Map <String, Double> result = new HashMap <> ();
    for (int i=0; i< Math.min(fbTerms, weightedTerms.size()); i++){
        result.put(weightedTerms.get(i).getTerm(), (1.0-fbOrigWeight)*weightedTerms.get(i).getWeight()/sum);
    }
    for (String qTerm : queryTerms){
        result.put(qTerm, fbOrigWeight/(double)queryTerms.size());
    }
    System.err.println("%%%%%%%%%%%% " + result.size());
    return result;
  }

  // this function is for special purpose and is not correct!
  public Map <String, Double> getPRFDist(Node root, Parameters queryParameters, List<ScoredDocument> initialResults) throws Exception {

    int fbDocs = (int) Math.round(root.getNodeParameters().get("fbDocs", queryParameters.get("fbDocs", (double) defaultFbDocs)));
    int fbTerms = (int) Math.round(root.getNodeParameters().get("fbTerm", queryParameters.get("fbTerm", (double) defaultFbTerms)));
    double fbOrigWeight = (int) Math.round(root.getNodeParameters().get("fbOrigWeight", queryParameters.get("fbOrigWeight", (double) defaultFbOrigWeight)));

    if (fbDocs <= 0 || fbTerms <= 0) {
      logger.info("fbDocs, or fbTerms is invalid, no expansion possible. (<= 0)");
      return null;
    }

    // transform query to ensure it will run
    Parameters fbParams = Parameters.create();
    fbParams.set("requested", fbDocs);
    // first pass is asserted to be document level
    fbParams.set("passageQuery", false);
    fbParams.set("extentQuery", false);
    fbParams.setBackoff(queryParameters);

    Node transformed = retrieval.transformQuery(root.clone(), fbParams);

    // extract grams from results
    Set<String> queryTerms = getTerms(stemmer, StructuredQuery.findQueryTerms(transformed));
    Set<String> exclusions = (fbParams.isString("rmstopwords")) ? WordLists.getWordList(fbParams.getString("rmstopwords")) : exclusionTerms;
    Set<String> inclusions = null;
    if (fbParams.isString("rmwhitelist")){
        inclusions = WordLists.getWordList(fbParams.getString("rmwhitelist"));
    } else {
        inclusions = inclusionTerms;
    }

    List<WeightedTerm> weightedTerms = extractGrams(retrieval, initialResults, stemmer, fbParams, queryTerms, exclusions, inclusions);

    double sum = 0;
    for (int i=0; i< Math.min(fbTerms, weightedTerms.size()); i++){
        if (queryTerms.contains(weightedTerms.get(i).getTerm()))
            continue;
        sum += weightedTerms.get(i).getWeight();
    }

    Map <String, Double> result = new HashMap <> ();
    for (int i=0; i< Math.min(fbTerms, weightedTerms.size()); i++){
        if (queryTerms.contains(weightedTerms.get(i).getTerm()))
            continue;
        result.put(weightedTerms.get(i).getTerm(), weightedTerms.get(i).getWeight()/sum);
    }
    return result;
  }

  public List<ScoredDocument> collectInitialResults(Node transformed, Parameters fbParams) throws Exception {
      Results results = retrieval.executeQuery(transformed, fbParams);
      List<ScoredDocument> res = results.scoredDocuments;
      return res;
  }

  public List<WeightedTerm> extractGrams(Retrieval retrieval, List<ScoredDocument> initialResults, Stemmer stemmer, Parameters fbParams, Set<String> queryTerms, Set<String> exclusionTerms, Set<String> inclusionTerms) throws IOException, Exception {
    // convert documentScores to posterior probs
    Map<ScoredDocument, Double> scores = logstoposteriors(initialResults);

    // get term frequencies in documents
    Map<String, Map<ScoredDocument, Integer>> counts = countGrams(retrieval, initialResults, stemmer, fbParams, queryTerms, exclusionTerms, inclusionTerms);

    // compute term weights
    List<WeightedTerm> scored = scoreGrams(counts, scores);

    // sort by weight
    Collections.sort(scored);

    return scored;
  }

  // Implementation here is identical to the Relevance Model unigram normaliztion in Indri.
  // See RelevanceModel.cpp for details
  public static Map<ScoredDocument, Double> logstoposteriors(List<ScoredDocument> results) {
    Map<ScoredDocument, Double> scores = new HashMap<ScoredDocument, Double>();
    if (results.isEmpty()) {
      return scores;
    }

    double[] values = new double[results.size()];
    for (int i = 0; i < results.size(); i++) {
      values[i] = results.get(i).score;

    }
    // compute the denominator
    double logSumExp = MathUtils.logSumExp(values);

    for (ScoredDocument sd : results) {
      double logPosterior = sd.score - logSumExp;
      scores.put(sd, Math.exp(logPosterior));
    }

    return scores;
  }

  public Map<String, Map<ScoredDocument, Integer>> countGrams(Retrieval retrieval, List<ScoredDocument> results, Stemmer stemmer, Parameters fbParams, Set<String> stemmedQueryTerms, Set<String> exclusionTerms, Set<String> inclusionTerms) throws IOException {
    Map<String, Map<ScoredDocument, Integer>> counts = new HashMap<String, Map<ScoredDocument, Integer>>();
    Map<ScoredDocument, Integer> termCounts;
    Document doc;

    Document.DocumentComponents corpusParams = new Document.DocumentComponents(true, false, true);

    String group = fbParams.get("group", (String) null);

    for (ScoredDocument sd : results) {
      if (group != null && retrieval instanceof GroupRetrieval) {
        doc = ((GroupRetrieval) retrieval).getDocument(sd.documentName, corpusParams, group);
      } else {
        doc = retrieval.getDocument(sd.documentName, corpusParams);
      }

      if (doc == null) {
        logger.info("Failed to retrieve document: " + sd.documentName + " -- RM skipping document.");
        continue;
      }

      List<String> docterms = doc.terms;

      sd.annotation = new AnnotatedNode();
      sd.annotation.extraInfo = "" + docterms.size();

      for (String term : docterms) {
        // perform stopword and query term filtering here //
        String stemmedTerm = (stemmer == null) ? term : stemmer.stem(term);
        if (inclusionTerms != null && !inclusionTerms.contains(term)) {
            continue; // not on the whitelist
        }
        if (stemmedQueryTerms.contains(stemmedTerm)){
            if (!queryCount.containsKey(stemmedTerm))
                queryCount.put(stemmedTerm, new HashMap <> ());
            if (!queryCount.get(stemmedTerm).containsKey(sd))
                queryCount.get(stemmedTerm).put(sd, 1);
            else
                queryCount.get(stemmedTerm).put(sd, queryCount.get(stemmedTerm).get(sd) + 1);
        }
        if (stemmedQueryTerms.contains(stemmedTerm) || exclusionTerms.contains(term)) {
          continue; // on the blacklist
        }
        if (!counts.containsKey(term)) {
          counts.put(term, new HashMap<ScoredDocument, Integer>());
        }
        termCounts = counts.get(term);
        if (termCounts.containsKey(sd)) {
          termCounts.put(sd, termCounts.get(sd) + 1);
        } else {
          termCounts.put(sd, 1);
        }
      }
    }
    return counts;
  }

    public List<WeightedTerm> scoreGrams(Map<String, Map<ScoredDocument, Integer>> counts, Map<ScoredDocument, Double> scores) throws IOException, Exception {
        List<WeightedTerm> grams = new ArrayList<WeightedTerm>();
        Map<ScoredDocument, Integer> termCounts;

        for (String term : counts.keySet()) {
            RelevanceModel1.WeightedUnigram g = new RelevanceModel1.WeightedUnigram(term);
            termCounts = counts.get(term);
            for (ScoredDocument sd : termCounts.keySet()) {
                g.score += scores.get(sd) * termProbability(counts, sd, term); //termCounts.get(sd) / length; // Relevance Model 1
            }
            // 1 / fbDocs from the RelevanceModel source code
            g.score *= (1.0 / scores.size());
            grams.add(g);
        }

        return grams;
    }

    public double termProbability (Map<String, Map<ScoredDocument, Integer>> counts, ScoredDocument sd, String term) throws Exception{
        // we forced this into the scored document earlier
        int length = Integer.parseInt(sd.annotation.extraInfo);
        return (double) counts.get(term).get(sd) / (double)length;
    }

    public static Set<String> getTerms(Stemmer stemmer, Set<String> terms) {
      if (stemmer == null)
          return terms;

      Set<String> stems = new HashSet<String>(terms.size());
      for (String t : terms) {
        String s = stemmer.stem(t);
        // stemmers should ensure that terms do not stem to nothing.
        stems.add(s);
      }
      return stems;
    }
}
