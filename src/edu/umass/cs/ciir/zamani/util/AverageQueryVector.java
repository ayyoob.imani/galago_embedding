/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
import edu.umass.cs.ciir.zamani.util.GalagoUtility;
import edu.umass.cs.ciir.zamani.util.MathUtility;
import edu.umass.cs.ciir.zamani.util.TermFactory;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.lemurproject.galago.core.retrieval.prf.WeightedTerm;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;

/**
 *
 * @author zamani
 */
public class AverageQueryVector implements QueryVector{
    //Node root;
    WordEmbedding we;
    Set <String> queryTerms;
    
    public AverageQueryVector(Node root, WordEmbedding we) {
        // pass transformed query to this!
        //this.root = root;
        this.we = we;
        queryTerms = GalagoUtility.setmTermsIfNeeded(StructuredQuery.findQueryTerms(root), null);
        for (String term : queryTerms) {
            if (!TermFactory.isStopword(term))
                this.queryTerms.add(term);
        }
    }
    
    public AverageQueryVector(String query, WordEmbedding we) {
        // pass transformed query to this!
        //this.root = root;
        String[] qTerms = query.split(" ");
        queryTerms = new HashSet <> ();
        for (String qTerm : qTerms)
            if (!TermFactory.isStopword(qTerm))
                queryTerms.add(qTerm.trim());
        this.we = we;
        
    }
    
    @Override
    public List<Double> getQueryVector() throws Exception {
        // pass transformed query to this!
        Set <String> embeddingVocab = we.getVocab();

        //Node x = retrieval.transformQuery(root, queryParameters);
        //Set <String> queryTerms = GalagoUtility.setmTermsIfNeeded(StructuredQuery.findQueryTerms(root), null);
        
        
        //System.err.println("# QUERY TERMS: " + queryTerms.size());
        for (String qTerm : queryTerms){
        //    System.err.println("######### " + qTerm);
            if (!embeddingVocab.contains(qTerm)){
                throw new Exception ("Vocabulary does not contain the query term: " + qTerm);
            }
        }
        if (queryTerms.size() == 0)
            return null;
        
        List <Double> avgQueryVec = new ArrayList <> ();
        for (String qTerm : queryTerms){
            List <Double> temp = we.getVector(qTerm);
            for (int i=0; i<temp.size(); i++){
                if (i >= avgQueryVec.size())
                    avgQueryVec.add(0.0);
                avgQueryVec.set(i, avgQueryVec.get(i) + temp.get(i) / queryTerms.size());
            }
        }
        
        return MathUtility.normalize(avgQueryVec);
    }
    
}
