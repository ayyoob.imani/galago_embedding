/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author zamani
 */
public class GeneralUtil {
    public static void TrecTextToPlainText (String fileName, String outputFileName) throws IOException, Exception{
        BufferedWriter writer = new BufferedWriter(new FileWriter (outputFileName));
        TrecTextToPlainText(new File(fileName), writer);
        writer.close();
    }

    private static void TrecTextToPlainText(File file, BufferedWriter writer) throws FileNotFoundException, IOException, Exception {
        if (file.isDirectory()){
            for (File f : file.listFiles())
                TrecTextToPlainText(f, writer);
        } else{
            Scanner reader = new Scanner (file);
            boolean flag = false;
            while(reader.hasNextLine()){
                String line = reader.nextLine();
                if (flag == false && line.contains("<TEXT>"))
                    flag = true;
                else if (flag == true && !line.contains("</TEXT>")){
                    if (!line.trim().equals("<P>") && !line.trim().equals("</P>")){
                        line = line.replaceAll("[,./-?`@#':;\\<>]", " ");
                        line = line.replace("(", " ");
                        line = line.replace(")", " ");
                        line = line.replace("\"", " ");
                        line = line.replace("'", " ");
                        line = line.replace("!", " ");
                        line = line.replace("[", " ");
                        line = line.replace("]", " ");
                        line = line.replace("{", " ");
                        line = line.replace("}", " ");
                        line = line.replace("-", " ");
                        line = line.replaceAll("[ ]+", " ");
                        writer.append(line.trim().toLowerCase() + " ");
                    }
                }
                else if (flag == true && line.contains("</TEXT>"))
                    flag = false;
                else if (flag == true && line.contains("<TEXT>"))
                    throw new Exception ("Something bad happened: " + line);
                else if (flag == false && line.contains("</TEXT>"))
                    throw new Exception ("Something bad happened: " + line);
            }
            reader.close();
        }
    }
    
    public static void executeScript(String script) {
        try {
            ProcessBuilder pb = new ProcessBuilder(script);
            Process p = pb.start(); // Start the process.
            p.waitFor(); // Wait for the process to finish.
            System.out.println("Script executed successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
