/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.util;

import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
import edu.umass.cs.ciir.zamani.util.GalagoUtility;
import edu.umass.cs.ciir.zamani.util.MathUtility;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;

/**
 *
 * @author zamani
 */
public class PseudoQueryVector implements QueryVector {
    protected WordEmbedding we;
    Map <String, Double> prfDist;
    double gamma;
    double defaultGamma = 10;
    double maxError = 0.000000001;
    int maxIter = 1000;
    Node root;
    Retrieval r;
    Map <String, List <Double>> queryTermVectors;
    
    
    public PseudoQueryVector(Retrieval r, Node root, WordEmbedding we, Map <String, Double> prfDist) throws Exception {
        this.we = we;
        this.prfDist = prfDist;
        this.gamma = defaultGamma;
        this.root = root;
        this.r = r;
        queryTermVectors = new HashMap <> ();
        Node x = r.transformQuery(root, Parameters.create());
        Set <String> queryTerms = GalagoUtility.setmTermsIfNeeded(StructuredQuery.findQueryTerms(x), null); // no stemmer
        for (String term : queryTerms)
            queryTermVectors.put(term, we.getVector(term));
    }
    
    
    @Override
    public List <Double> getQueryVector () throws Exception{
        List <Double> result = new ArrayList <> ();
        for (int i=0; i<we.getDimension(); i++)
            result.add(0.0);
        /*
        Map <String, Double> allDotProducts = new HashMap <> ();
        for (String qterm : prfDist.keySet()){
            List <Double> qVec = we.getVector(qterm);
            if (qVec == null)
                continue;
            double val = 0;
            for (String term : we.getVocab()){
                if (term.equals(qterm))
                    continue;
                List <Double> termVec = we.getVector(term);
                val += MathUtility.modifiedDotProduct(qVec, termVec);
            }
            allDotProducts.put(qterm, val);
        }*/
        
        for (String term : prfDist.keySet()){
            List <Double> vec = we.getVector(term);
            if (vec == null)
                continue;
            for (int i=0; i<we.getDimension(); i++){
                result.set(i, result.get(i) + (prfDist.get(term)/*/ allDotProducts.get(term)*/) * vec.get(i));
            }
        }
        
        return MathUtility.normalize(result);
    }
    
    
    
    //@Override
    public List <Double> getQueryVector2 () throws Exception{
        int dim = we.getDimension();
        double maxLikelihood = Double.NEGATIVE_INFINITY;
        List <Double> result = new ArrayList <> ();
        
        List <List <Double>> initialVectors = new ArrayList <> ();
        /*
        initialVectors.add(new HashMap <> ());
        for (String term : queryTermVectors.keySet()){
            initialVectors.get(0).put(term, 1.0/qlen);
        }*/

        // initializing with average query vector!
        List <Double> temp = new ArrayList <> ();
        for (int i=0; i<we.getDimension(); i++)
            temp.add(0.0);
        
        for (String term : prfDist.keySet()){
            List <Double> vec = we.getVector(term);
            if (vec == null)
                continue;
            for (int i=0; i<we.getDimension(); i++){
                temp.set(i, temp.get(i) + prfDist.get(term) * vec.get(i));
            }
        }
        
        
        //initialVectors.add(new AverageQueryVector(r.transformQuery(root.clone(), Parameters.create()), we).getQueryVector());
        initialVectors.add(temp);
        // initializing with query term vectors
        //initialVectors.addAll(getQueryTermVectors());
        
        for (List initialVector : initialVectors){
            // initializing with average query vector!
            //List <Double> queryVec = new AverageQueryVector(r.transformQuery(root.clone(), Parameters.create()), we).getQueryVector();
            //Map <String, Double> alphaVec = initialVector;
            List <Double> queryVec = new ArrayList<> ();
            queryVec.addAll(initialVector);
            List <Double> newVec = new ArrayList<> ();
            for (int i=0; i<dim; i++)
                newVec.add(0.0);
            /*Map <String, Double> newAlphaVec = new HashMap<> ();
            for (String term : queryTermVectors.keySet())
                newAlphaVec.put(term, 0.0);
            */
            // do gradient ascent
            double error = Double.MAX_VALUE;
            int iter = 0;
            System.err.print("[");
            while (error > maxError && iter < maxIter){
                gamma *= 0.99;
                if (gamma < 0.1)
                    gamma = 0.1;
                System.err.println("Gamma: " + gamma);
                List <Double> gradient = getGradient(queryVec);
                double maxVal = Collections.max(gradient);
                for (int i=0; i<dim; i++){
                    double newVal;
                    if (gradient.get(i) == maxVal)
                        newVal = queryVec.get(i) + gamma * gradient.get(i);
                    else
                        newVal = queryVec.get(i);
                    System.err.print(" " + queryVec.get(i));
                    newVec.set(i, newVal);
                }
                System.err.println("\n************* " + loglikelihood(queryVec));
                //newAlphaVec = MathUtility.normalize(newAlphaVec);
                error = 1.0 - MathUtility.modifiedDotProduct(MathUtility.normalize(newVec), MathUtility.normalize(queryVec));
                queryVec.clear();
                queryVec.addAll(newVec);
                
                System.err.print(".");
                System.err.println(error);
                iter++;
                //for (double d : newVec)
                //System.err.print(d + " ");
                //System.err.println();
            }
            System.err.println("]");
            System.err.println("#iterations: " + iter);
            
            double likelihood = loglikelihood(newVec);
            if (likelihood > maxLikelihood){
                maxLikelihood = likelihood;
                result = newVec;
            }
        }
        return MathUtility.normalize(result);
    }
    
    public double loglikelihood(List <Double> vec) throws Exception{
        double result = 0;
        for (String term : prfDist.keySet()){
            if (we.getVocab().contains(term)){
                result += (prfDist.get(term) * Math.log(we.getSimilarity(vec, we.getVector(term), false)));
            }
        }
        return result;
    }

    protected List <Double> getGradient(List <Double> alphaVec) throws Exception {
        throw new UnsupportedOperationException("It's the parent class! Use one of the children!");
    }

    
    protected List <Double> getQueryVector(Map <String, Double> alphaVec) throws Exception {
        List <Double> result = new ArrayList <> ();
        if (alphaVec.size() != queryTermVectors.size())
            throw new Exception ("Something is wrong!");
        for (int i=0; i<we.getDimension(); i++){
            result.add(0.0);
        }
        for (String term : queryTermVectors.keySet()){
            for (int j=0; j<we.getDimension(); j++){
                result.set(j, result.get(j) + alphaVec.get(term) * queryTermVectors.get(term).get(j));
            }
        }
        return result;
    }
}
