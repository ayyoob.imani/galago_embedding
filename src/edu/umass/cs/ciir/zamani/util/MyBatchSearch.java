/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.umass.cs.ciir.zamani.util;
import edu.umass.cs.ciir.zamani.prf.WordEmbedding;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.lemurproject.galago.core.index.stats.NodeStatistics;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.prf.ExpansionModel;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import static org.lemurproject.galago.core.tools.apps.BatchSearch.logger;
import org.lemurproject.galago.core.util.WordLists;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;

/**
 *
 * @author zamani
 */
public class MyBatchSearch {
    
    public static void main(String[] args) throws Exception{
    //    GeneralUtil.TrecTextToPlainText("/home/zamani/Desktop/datasets/raw/robust04", "/mnt/scratch/plain/robust04.txt");
        String dataset = "AP";
        String[] indexPaths = new String[1];
        if (dataset == "AP")
            indexPaths[0] = "/home/reyhane/IR/AP/galagoIndex"; //"/mnt/scratch/galago-index/nostem/" + dataset;
        else
            indexPaths[0] = "/mnt/scratch/galago-index/stemmed/" + dataset;

        String queryFileName = "/home/reyhane/IR/AP/queries/queries.xml";
        TermFactory.create(indexPaths, true);
        /*
        String indexPath = "/mnt/scratch/idx";//"/mnt/scratch/galago-index/nostem/" + dataset;
        String[] indexPaths = {"/mnt/scratch/idx/nyt87_fields.idx",
        "/mnt/scratch/idx/nyt88_fields.idx",
        "/mnt/scratch/idx/nyt89_fields.idx",
        "/mnt/scratch/idx/nyt90_fields.idx",
        "/mnt/scratch/idx/nyt91_fields.idx",
        "/mnt/scratch/idx/nyt92_fields.idx",
        "/mnt/scratch/idx/nyt93_fields.idx",
        "/mnt/scratch/idx/nyt94_fields.idx",
        "/mnt/scratch/idx/nyt95_fields.idx",
        "/mnt/scratch/idx/nyt96_fields.idx",
        "/mnt/scratch/idx/nyt97_fields.idx",
        "/mnt/scratch/idx/nyt98_fields.idx",
        "/mnt/scratch/idx/nyt99_fields.idx",
        "/mnt/scratch/idx/nyt00_fields.idx",
        "/mnt/scratch/idx/nyt01_fields.idx",
        "/mnt/scratch/idx/nyt02_fields.idx",
        "/mnt/scratch/idx/nyt03_fields.idx",
        "/mnt/scratch/idx/nyt04_fields.idx",
        "/mnt/scratch/idx/nyt05_fields.idx",
        "/mnt/scratch/idx/nyt06_fields.idx",
        "/mnt/scratch/idx/nyt07_fields.idx"};
        String queryFileName = "/mnt/scratch/queries/" + dataset + "/topics.txt";
        TermFactory.create(indexPaths, false);*/
            String outputFileName = "/home/reyhane/lemur-galago/out/output.txt";
        new MyBatchSearch().retrieve(indexPaths, queryFileName, outputFileName);
        //new MyBatchSearch().paramSensitivity(indexPath, queryFileName);
    }

    public void retrieve(String[] indexPath, String queryFileName, String outputFileName) throws Exception {
        List<ScoredDocument> results;
        int requested = 2000;
        boolean append = false;
        boolean queryExpansion = false;
        //ResultWriter resultWriter = new ResultWriter(outputFileName, append);
        // open index
        Retrieval retrieval = RetrievalFactory.instance(Arrays.asList(indexPath), Parameters.create());
        // read word embedding vectors
//        int dim = 200;
//        WordEmbedding we = WordEmbedding.getInstance("/home/zamani/Desktop/projects/PRF_Embedding/code/data/vectors.6B." + dim + "d.txt" , dim);
        //WordEmbedding we = WordEmbedding.getInstance("/home/zamani/Desktop/projects/PRF_Embedding/code/data/glove.840B.300d.txt" , dim);
      /*  for (String t : we.getVocab()){
            if (t.equals("book"))
                continue;
            System.err.println(we.getSimilarity(t, "book"));
        }
  //      we.printSimilarTerms("/mnt/scratch/vectors/sims.txt");
        if (true)
            return;
      */  
        //WordEmbedding we = WordEmbedding.getInstance("/mnt/scratch/vectors/glove-robust04-200d.txt" , dim);
        // get queries
        
//        List<Parameters> queries = getQueries(retrieval, we, QueryParser.trecToGalagoFormat(queryFileName, retrieval, true));//.subList(0, 50);
        List<Parameters> queries = QueryParser.trecToGalagoFormat(queryFileName, retrieval, true);
//        for (double c=0.7; c<0.9001; c+=0.05){
//            we.setA(5);
//            we.setC(c);
//        ResultWriter resultWriter = new ResultWriter("/home/zamani/Desktop/res400-" + c + ".txt", append);
        ResultWriter resultWriter = new ResultWriter(outputFileName, append);

        // for each query, run it, get the results, print in TREC format
        List <Long> timeArray = new ArrayList <> ();
        long start1 = System.currentTimeMillis();
        for (Parameters query : queries) {
            //System.err.println(retrieval.getNodeStatistics(query.getString("text")).nodeFrequency);
            //if (true) return;
            long start = System.currentTimeMillis();
            String queryNumber = query.getString("number");
            String queryText = query.getString("text");
            queryText = queryText.toLowerCase(); // option to fold query cases -- note that some parameters may require upper case
            
            logger.info("Processing query #" + queryNumber + ": " + queryText);
            

            //query.setBackoff(parameters);
            query.set("requested", requested);
//            query.set("uniw", 0.9);
//            query.set("odw", 0.05);
//            query.set("uww", 0.05);

            // parse and transform query into runnable form
            Node root = StructuredQuery.parse(queryText);
            Node transformed = retrieval.transformQuery(root, query);
            
            
            // Query Expansion
            if (queryExpansion){
                ExpansionModel qe = new org.lemurproject.galago.core.retrieval.prf.RelevanceModel3(retrieval);
//                EmbeddingRelevanceModel2 qe = new EmbeddingRelevanceModel2(retrieval, we);
//                ExpansionModel qe = new EmbeddingModel1(retrieval, we);
                //ExpansionModel qe = new AlMasriBaseline(retrieval, we);
//                EmbeddingRelevanceModel2 qe = new EmbeddingRelevanceModel2(retrieval, we);
                try{
                    query.set("fbOrigWeight", 0.9);
                    query.set("fbTerm", 30.0);
                    Node expandedQuery = qe.expand(root.clone(), query.clone());  
                    transformed = retrieval.transformQuery(expandedQuery, query);
                } catch (Exception ex){
                    ex.printStackTrace();
                }
                //Node expandedQuery2 = qe2.expand(transformed.clone(), query.clone());    
                //query.set("defaultFbOrigWeight", 0.3);
                //Node finalQuery = new LinearInterpolator().interpolate(expandedQuery2, expandedQuery, query.clone());
                //qe = new RelevanceModel3(retrieval);
                //ExpansionModel qe = new EmbeddingRelevanceModel1(retrieval, WordEmbedding.getInstance("/home/zamani/Desktop/projects/PRF_Embedding/code/data/glove.840B.300d.txt" , dim));
                
                //qe = new RelevanceModel3(retrieval);
                //expandedQuery = qe.expand(root.clone(), query.clone());
                
            }
            //logger.info("Transformed Query:\n" + transformed.toPrettyString());
            
            // run query
            results = retrieval.executeQuery(transformed, query).scoredDocuments;
            
            // print results
            resultWriter.write(queryNumber, results);
            timeArray.add(System.currentTimeMillis() - start);
        }
        resultWriter.close();
        System.err.println(Collections.max(timeArray));
        System.err.println(Collections.min(timeArray));
        double average = 0;
        for (int i=0; i<timeArray.size();i++)
            average += ((double)timeArray.get(i))/timeArray.size();
        System.err.println(average);
        double sd = 0;
        for (int i=0; i<timeArray.size();i++) {
            //this is where im having problems
            {
                sd += ((timeArray.get(i) - average)*(timeArray.get(i) - average)) / (timeArray.size() - 1);
            }
        }
        double standardDeviation = Math.sqrt(sd);
        System.err.println(standardDeviation);
        System.err.println(System.currentTimeMillis() - start1);
    }
    
    
    public void paramSensitivity(String indexPath, String queryFileName) throws Exception {
        List<ScoredDocument> results;
        int requested = 1000;
        boolean append = false;
        boolean queryExpansion = true;
        
        
        // open index
        Retrieval retrieval = RetrievalFactory.instance(indexPath);
        
        // read word embedding vectors
        int dim = 200;
        WordEmbedding we = WordEmbedding.getInstance("/home/zamani/Desktop/projects/PRF_Embedding/code/data/vectors.6B." + dim + "d.txt" , dim);
        /*for (String t : we.getVocab()){
            if (t.equals("book"))
                continue;
            System.err.println(we.getSimilarity(t, "book"));
        }
  //      we.printSimilarTerms("/mnt/scratch/vectors/sims.txt");
        if (true)
            return;
        */
        //WordEmbedding we = WordEmbedding.getInstance("/mnt/scratch/vectors/glove-robust04-200d.txt" , dim);
        // get queries
        List<Parameters> queries = getQueries(retrieval, we, QueryParser.trecToGalagoFormat(queryFileName, retrieval, true));//.subList(0, 50);
        
        //int []fbTermCounts = {10, 25, 50, 75, 100};
        //for (int n=5; n<51; n+=5){
        for (double alpha = 0.8; alpha<0.91; alpha += 0.02){
            // for each query, run it, get the results, print in TREC format
            we.setC(alpha);
            //we.setA(0.1);
            ResultWriter resultWriter = new ResultWriter("/home/zamani/Desktop/gov2-c-" + alpha + ".txt", append);
            for (Parameters query : queries) {
                String queryNumber = query.getString("number");
                String queryText = query.getString("text");
                queryText = queryText.toLowerCase(); // option to fold query cases -- note that some parameters may require upper case

                logger.info("Processing query #" + queryNumber + ": " + queryText);


                //query.setBackoff(parameters);
                query.set("requested", requested);

                // parse and transform query into runnable form
                Node root = StructuredQuery.parse(queryText);
                Node transformed = retrieval.transformQuery(root, query);

                // Query Expansion
                if (queryExpansion){
              //      EmbeddingModel1 qe = new EmbeddingModel1(retrieval, we);
                    //qe.setFbTermCount(n);
                    //qe.setBeta(alpha);
                    //ExpansionModel qe = new RelevanceModel3(retrieval);
                   // ExpansionModel qe2 = new EmbeddingModel2(retrieval, we);
               //     Node expandedQuery = qe.expand(root.clone(), query.clone());
                    //Node expandedQuery2 = qe2.expand(transformed.clone(), query.clone());    
                    //query.set("defaultFbOrigWeight", 0.3);
                    //Node finalQuery = new LinearInterpolator().interpolate(expandedQuery2, expandedQuery, query.clone());
                    //qe = new RelevanceModel3(retrieval);
                    //ExpansionModel qe = new EmbeddingRelevanceModel1(retrieval, WordEmbedding.getInstance("/home/zamani/Desktop/projects/PRF_Embedding/code/data/glove.840B.300d.txt" , dim));

                    //qe = new RelevanceModel3(retrieval);
                    //expandedQuery = qe.expand(root.clone(), query.clone());
               //     transformed = retrieval.transformQuery(expandedQuery, query);
                }
                //logger.info("Transformed Query:\n" + transformed.toPrettyString());

                // run query
                results = retrieval.executeQuery(transformed, query).scoredDocuments;

                // print results
                resultWriter.write(queryNumber, results);
            }
            resultWriter.close();
        }
    }
    
    
    public static List <Parameters> getQueries (Retrieval r, WordEmbedding we, List <Parameters> queries) throws Exception{
        List <Parameters> result = new ArrayList <> ();  
        System.err.println(queries.size());
        for (Parameters query : queries){
            Node root = StructuredQuery.parse(query.getString("text").toLowerCase().replace("/", " ").replace("'", " "));
            root = r.transformQuery(root, query);
            Set <String> queryTerms = GalagoUtility.setmTermsIfNeeded(StructuredQuery.findQueryTerms(root), null);
            try{
                for (String qTerm : queryTerms){
                    if (!we.getVocab().contains(qTerm)){// && !TermFactory.isStopword(qTerm)){
                        throw new Exception ("Query #" + query.getString("number") + ": Vocabulary does not contain the query term: " + qTerm);
                    }
                }
            } catch (Exception ex){
                ex.printStackTrace();
                continue;
            }
            result.add(query);
        }
        System.err.println(result.size());
        return result;
    }
}
