package edu.umass.cs.ciir.zamani.util;

import edu.umass.cs.ciir.zamani.prf.WordEmbedding;

/**
 * Created by vandermonde on 9/28/18.
 */
public class ExtractAllSimilarities {
    public static void main(String[] args) throws Exception{
        //    GeneralUtil.TrecTextToPlainText("/home/zamani/Desktop/datasets/raw/robust04", "/mnt/scratch/plain/robust04.txt");
        String dataset = "AP";
        String indexPath = "/home/iis/Documents/AP/index/galago/IndexAp";
        String outputFileName = "/home/vandermonde/Documents/galago/outputTopic.txt";
        String queryFileName = "/home/vandermonde/Documents/galago/galago/queries.xml";
        TermFactory.create(indexPath);
        new ExtractAllSimilarities().runExtraction();
    }

    public void runExtraction() throws Exception {
        int dim = 200;
        WordEmbedding we = WordEmbedding.getInstance("/home/iis/word2vec_ngram/glove.6B/glove.6B.200d.txt", dim);
        we.computeAllSimilarity("/home/iis/Documents/zamani/allsims/similarities200d_a8_c.85");
    }
}
