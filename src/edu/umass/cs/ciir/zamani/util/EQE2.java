package edu.umass.cs.ciir.zamani.util;

import edu.umass.cs.ciir.zamani.prf.LinearInterpolator;
import edu.umass.cs.ciir.zamani.prf.WordEmbedding;
import org.lemurproject.galago.core.parse.stem.Stemmer;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.prf.ExpansionModel;
import org.lemurproject.galago.core.retrieval.prf.RelevanceModel1;
import org.lemurproject.galago.core.retrieval.prf.WeightedTerm;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class EQE2 implements ExpansionModel{
    private static final Logger logger = Logger.getLogger("EMB1");
    protected int defaultFbTerms;
    protected Retrieval retrieval;
    protected Set<String> exclusionTerms;
    protected Stemmer stemmer;
    protected WordEmbedding wordEmbedding;
    protected double defaultFbOrigWeight;

    public EQE2(Retrieval r, WordEmbedding we, Parameters param) throws Exception {
        this.retrieval = r;
        defaultFbTerms = (int) Math.round(r.getGlobalParameters().get("fbTerm", param.get("fbterms", 30)));
        this.exclusionTerms = TermFactory.stopWordList();
        Parameters gblParms = r.getGlobalParameters();
        this.stemmer = GalagoUtility.getStemmer(gblParms, retrieval);
        wordEmbedding = we;
        defaultFbOrigWeight = r.getGlobalParameters().get("fbOrigWeight", param.get("fbOrigWeight", 0.75));

    }


    @Override
    public Node expand(Node root, Parameters queryParameters) throws Exception {
        return null;
    }

    public Node expand(Node root, Parameters queryParameters, BufferedWriter outFile) throws Exception {
        Set<String> embeddingVocab = wordEmbedding.getVocab();
        int fbTerms = (int) Math.round(root.getNodeParameters().get("fbTerm", queryParameters.get("fbTerm", (double) defaultFbTerms)));

        if (fbTerms <= 0) {
            logger.info("fbTerms is invalid, no expansion possible. (<= 0)");
            return root;
        }
        List<WeightedTerm> expansionTerms = new ArrayList<>();

        Node x = retrieval.transformQuery(root, queryParameters);
        Set<String> queryTerms = StructuredQuery.findQueryTerms(x);

        // if only a term of query does not exist in vocab we should ignore it !
        System.err.println("# QUERY TERMS: " + queryTerms.size());
        outFile.write(queryParameters.getString("number"));
        for (String qTerm : queryTerms) {
            System.err.println("######### " + qTerm);
            outFile.write("," + qTerm);
            if (!embeddingVocab.contains(qTerm)) {
//                throw new Exception("Vocabulary does not contain the query term: " + qTerm);
                System.out.println("the query left without embedding !!!\nho ho\n");
                return root;
            }
        }
        outFile.write("\n");
        if (queryTerms.size() == 0)
            return root;

        for (String term : embeddingVocab) {
            // skip query terms and blacklist words
            if (queryTerms.contains(term) || exclusionTerms.contains(term) || !TermFactory.isValidTerm(term)) {
                continue; // on the blacklist
            }
            double sum = 0.0;
            for (String queryTerm : queryTerms) {
                sum = sum + (wordEmbedding.getSimilarityProb(term, queryTerm) * 1.0);
            }
            expansionTerms.add(new RelevanceModel1.WeightedUnigram(term, sum));
        }

        Collections.sort(expansionTerms);

        Node expNode = generateExpansionQuery(expansionTerms, fbTerms, outFile);
        queryParameters.set("defaultFbOrigWeight", defaultFbOrigWeight);
        return new LinearInterpolator().interpolate(root, expNode, queryParameters);

    }

    public Node generateExpansionQuery(List<WeightedTerm> weightedTerms, int fbTerms, BufferedWriter outFile) throws IOException, Exception {
        Node expNode = new Node("combine");
        double sum = 0;
        for (int i = 0; i < Math.min(weightedTerms.size(), fbTerms); i++) {
            sum += weightedTerms.get(i).getWeight();
        }

        for (int i = 0; i < Math.min(weightedTerms.size(), fbTerms); i++) {
            Node expChild = new Node("text", weightedTerms.get(i).getTerm());
            expNode.addChild(expChild);
            double w = weightedTerms.get(i).getWeight() / sum;
            expNode.getNodeParameters().set("" + i, w);
            outFile.write(weightedTerms.get(i).getTerm() + "," + w+"\n");
        }
        outFile.write("\n");
        return expNode;
    }
}

