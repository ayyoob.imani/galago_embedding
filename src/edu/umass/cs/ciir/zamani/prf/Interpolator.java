/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.prf;

import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;

/**
 *
 * @author zamani
 */
public interface Interpolator {
    public Node interpolate (Node root, Node expNode, Parameters parameters) throws Exception;
}
