/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.umass.cs.ciir.zamani.prf;

import edu.umass.cs.ciir.zamani.util.MathUtility;
import edu.umass.cs.ciir.zamani.util.TermFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.lemurproject.galago.core.parse.stem.Stemmer;

/**
 * @author zamani
 */
public class WordEmbedding {
    private static final Logger logger = Logger.getLogger("Word Embedding");
    Map<String, List<Double>> embeddingVectors;
    //Map <String, List <String>> nearestNeighbor;
    Map<String, Double> allSimilarity;
    boolean allSimilaritiesChanged = false;
    //List <Double> all;
    double a=8.0, c=0.85;
//    double a = 2, c = 0.8;
    int dim;
    static WordEmbedding instance = null;
    Stemmer stemmer = (Stemmer) Class.forName("org.lemurproject.galago.core.parse.stem.KrovetzStemmer").getConstructor().newInstance();

    public static WordEmbedding getInstance(String fileName, String allSimFileName, int dim) throws Exception {
        if (instance == null)
            instance = new WordEmbedding(fileName, allSimFileName, dim);
        return instance;
    }

    public static WordEmbedding getInstance(String fileName, int dim) throws Exception {
        if (instance == null)
            instance = new WordEmbedding(fileName, null, dim);
        return instance;
    }

    public static WordEmbedding getInstance() throws Exception {
        if (instance == null)
            throw new Exception("Instance is not yet created!");
        return instance;
    }

    private WordEmbedding(String fileName, String allSimFile, int dim) throws Exception {
        this.dim = dim;
        embeddingVectors = new HashMap<>(400000);
        allSimilarity = new ConcurrentHashMap<>();
        readEmbeddingFile(fileName);
        if (allSimFile != null)
            readAllSimilarityFile(allSimFile);
    }

    public int getDimension() {
        return dim;
    }

    public Set<String> getVocab() {
        return embeddingVectors.keySet();
    }

    public void setAllSimilarityChanged(boolean val){
        allSimilaritiesChanged = val;
    }

    public double getAllSimilarity(String term) throws Exception {
        /*double result = 0;
        if (nearestNeighbor.containsKey(term)){
            for (String neighbor : nearestNeighbor.get(term))
                result += getSimilarity(term, neighbor);
        }
        return result;
        *///return MathUtility.modifiedDotProduct(embeddingVectors.get(term), MathUtility.vectorSubtract(all, embeddingVectors.get(term))) + (0.5*embeddingVectors.size()-1);
        if (allSimilarity.containsKey(term))
            return allSimilarity.get(term);
        else {
            if(embeddingVectors.keySet().contains(term)){

                double sum = 0;
                for (String str : embeddingVectors.keySet()) {
                    if (term.equals(str))
                        continue;
                    sum += getSimilarity(term, str);
                }
                if (sum == 0) {
                    throw new Exception("Something wrong happened!");
                }
                allSimilarity.put(term, sum);
                allSimilaritiesChanged = true;
                System.out.println("calculated allsims for " + term);
                return sum;

            } else
                throw new Exception("Vocabulary of all similarities does not contain the query term: " + term);
        }


    }

    public double getSimilarity(String aa, String b) throws Exception {
        if (!embeddingVectors.containsKey(aa) || !embeddingVectors.containsKey(b))
            return 0;//throw new Exception ("Word " + a + " or " + b + " does not exist in the embedding vocabulary!");
        return getSimilarity(embeddingVectors.get(aa), embeddingVectors.get(b), false);
    }

    public double getSimilarity(List<Double> aa, List<Double> b) throws Exception {
        double mult = 1;
        //if (nearestNeighbor.containsKey(a) && nearestNeighbor.containsKey(b) && (nearestNeighbor.get(a).contains(b) || nearestNeighbor.get(b).contains(a)))
        //    mult *= 1;
        double cosine = mult * MathUtility.modifiedDotProduct(aa, b);
        //a = 8;
        //c = 0.85;
//        return 1.0 / (1.0 + Math.exp(-a * (cosine - c)));
        return cosine;
        //return Math.exp(cosine);
    }

    public double getSimilarity(List<Double> aa, List<Double> b, boolean normalized) throws Exception {
        if (normalized == true)
            return getSimilarity(aa, b);
        List normalized_a = MathUtility.normalize(aa);
        List normalized_b = MathUtility.normalize(b);
        return getSimilarity(normalized_a, normalized_b);
    }

    // p(a|b)
    public double getSimilarityProb(String a, String b) throws Exception {
        return getSimilarity(a, b) / getAllSimilarity(b) - 1;
    }

    private void readEmbeddingFile(String fileName) throws FileNotFoundException, Exception {
        Scanner in = new Scanner(new File(fileName), "UTF-8");
        int c = 0;
//        ArrayList <String> uselessTerms= new ArrayList<>();

        Integer oovterms = 0;
        Integer problemousTerms = 0;
        Integer iii =0;

        String[] line;
        while(in.hasNextLine()){
            iii++;
            if(iii%1000 == 0)
                System.out.println("read lines: " + iii);
             line = in.nextLine().trim().split("[ \t]");
            if (line.length != dim + 1) {
                problemousTerms += 1;
                System.err.println("Something is wrong with the embedding file! " + line.length + " " + line[0]);
                continue;
            }

            line[0] = line[0].trim();
            // Note: some terms can be ignored based on exclusion term list! I'll handle them when I am using this class! (for instance, for PRF)
//            if (!TermFactory.isValidTerm(stemmer.stem(term)) || TermFactory.isStopword(stemmer.stem(term))) {
////                uselessTerms.add(term);
//                oovterms += 1;
//                continue;
//            }

            List<Double> vec = new ArrayList<>();
            for (int i = 1; i < line.length; i++) {
                vec.add(Double.parseDouble(line[i]));
            }
            if (embeddingVectors.containsKey(line[0])) {
                for (String s : line) {
                    System.out.print(s + "  ");
                }
                System.out.println("t ");
                throw new Exception("The term " + line[0] + " exists in the embedding vector! " + iii);

            }
            embeddingVectors.put(line[0], MathUtility.normalize(vec));
            //all = MathUtility.vectorAdd(all, embeddingVectors.get(term));
        }

        System.out.println("oov term count " + oovterms.toString() + "problemous terms: " + problemousTerms.toString());
        in.close();
        logger.info(embeddingVectors.size() + " embedding vectors have been successfully read!");

//        BufferedWriter writer = new BufferedWriter(new FileWriter("/home/reyhane/IR/glove.6B/uselessTerms.txt"));
//        for (String term : uselessTerms){
//            writer.write(term + "\n");
//        }
//        writer.close();


    }

    private void readAllSimilarityFile(String fileName) throws FileNotFoundException, Exception {
        Scanner in = new Scanner(new File(fileName));

        while (in.hasNextLine()) {
            String[] line = in.nextLine().trim().split("[ \t]");
            if (line.length != 2) {
                System.err.println("Something is wrong with the embedding file! " + line.length + " " + line[0]);
                continue;
            }
            String term = line[0];
            // Note: some terms can be ignored based on exclusion term list! I'll handle them when I am using this class! (for instance, for PRF)
            if (!TermFactory.isValidTerm(stemmer.stem(term)) || TermFactory.isStopword(stemmer.stem(term))) {
                continue;
            }

            Double simToAllWords = Double.parseDouble(line[1]);
            if (allSimilarity.containsKey(term))
                throw new Exception("The term " + term + " exists in the embedding vector!");
            allSimilarity.put(term, simToAllWords);
        }
        in.close();
        logger.info(allSimilarity.size() + " all similarity have been successfully read!");

    }

    public void computeAllSimilarity(String fileName) throws FileNotFoundException, Exception {
        fileName += ".allsim-final";
        File file = new File(fileName);
        if (file.exists()) {
            Scanner in = new Scanner(file);
            while (in.hasNext()) {
                String term = in.next();
                double sim = in.nextDouble();
                allSimilarity.put(term, sim);
            }
            in.close();
        }
        int threadNum = Runtime.getRuntime().availableProcessors();
        logger.info(threadNum + " threads are running for computing embedding similarities.");
        ExecutorService exec = Executors.newFixedThreadPool(threadNum);
        boolean flag = false;
        try {
            for (final String term : embeddingVectors.keySet()) {
                if (allSimilarity.containsKey(term))
                    continue;
                flag = true;
                exec.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            double sum = 0;
                            for (String str : embeddingVectors.keySet()) {
                                if (term.equals(str))
                                    continue;
                                sum += getSimilarity(term, str);
                            }
                            if (sum == 0) {
                                throw new Exception("Something wrong happened!");
                            }
                            allSimilarity.put(term, sum);
                            if (allSimilarity.size() % 100 == 0)
                                logger.info("Similarity between " + allSimilarity.size() + " out of " + embeddingVectors.size() + " embedding vectors and all embedding other vectors have been so far computed!");
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        } finally {
            exec.shutdown();
        }
        int hour = 20;
        logger.info("waiting for at most " + hour + " hours to compute these similarities.");
        boolean finished = exec.awaitTermination(hour, TimeUnit.HOURS);
        logger.info("All threads are terminated with this status: " + finished);

        if (flag) {
            PrintWriter pw = new PrintWriter(new File(fileName));
            Iterator<Map.Entry<String, Double>> it = allSimilarity.entrySet().iterator();
            while (it.hasNext()) {
                Entry entry = it.next();
                pw.println(entry.getKey() + " " + entry.getValue());
            }
            pw.close();
        }
        logger.info("Similarity between all embedding vectors has been computed!");
    }

    public void writeAllSims(String fileName) throws FileNotFoundException {
        if(allSimilaritiesChanged) {
            PrintWriter pw = new PrintWriter(new File(fileName));
            Iterator<Map.Entry<String, Double>> it = allSimilarity.entrySet().iterator();
            while (it.hasNext()) {
                Entry entry = it.next();
                pw.println(entry.getKey() + " " + entry.getValue());
            }
            pw.close();
        }

    }

    public Map<String, Double> getSumAllSimilarityForTermList(Map<String, Integer> terms) throws FileNotFoundException, Exception {
        Map<String, Double> result = new ConcurrentHashMap<>();
        int threadNum = Runtime.getRuntime().availableProcessors();
        ExecutorService exec = Executors.newFixedThreadPool(threadNum);
        try {
            for (final String term : terms.keySet()) {
                exec.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            double sum = 0;
                            for (String str : terms.keySet()) {
                                if (term.equals(str))
                                    continue;
                                sum += getSimilarity(term, str);//*terms.get(str);
                            }
                            if (sum == 0) {
                                return;
                            }
                            result.put(term, sum);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        } finally {
            exec.shutdown();
        }
        boolean finished = exec.awaitTermination(1, TimeUnit.MINUTES);
        if (!finished)
            logger.info("All threads are terminated with this status: " + finished);
        return result;
    }

    public List<Double> getVector(String term) {
        if (!embeddingVectors.containsKey(term))
            return null;
        return embeddingVectors.get(term);
    }

//    public void printSimilarTerms(String outputFileName) throws IOException, Exception{
//        PrintWriter printer = new PrintWriter(new File(outputFileName));
//        for (String term : embeddingVectors.keySet()){
//            List <WeightedTerm> list = new ArrayList <> ();
//            for (String t : embeddingVectors.keySet()){
//                if (term.equals(t))
//                    continue;
//                list.add(new RelevanceModel1.WeightedUnigram(t, getSimilarity(term, t)));
//            }
//            Collections.sort(list);
//            printer.print(term + " ");
//            for (int i=0; i<100; i++){
//                printer.print(list.get(i).getTerm() + " ");
//            }
//            printer.println();
//        }
//        printer.close();
//    }

    public void setA(double n) {
        a = n;
    }

    public void setC(double alpha) {
        c = alpha;
    }

    public Double getA() {
        return a;
    }

    public Map<String, Double> getAllSimilarities(List<Double> queryVec) throws Exception {
        List normalizedQueryVec = MathUtility.normalize(queryVec);
        Map<String, Double> result = new HashMap<>();
        for (String term : embeddingVectors.keySet()) {
            result.put(term, getSimilarity(normalizedQueryVec, embeddingVectors.get(term)));
        }
        return result;
    }


}
 