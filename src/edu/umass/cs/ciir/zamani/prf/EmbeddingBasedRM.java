//package edu.umass.cs.ciir.zamani.prf;
//
//import cc.mallet.types.StringKernel;
//import edu.umass.cs.ciir.zamani.util.GalagoUtility;
//import org.lemurproject.galago.core.retrieval.prf.ExpansionModel;
//import org.lemurproject.galago.core.retrieval.prf.RelevanceModel1;
//import org.lemurproject.galago.core.parse.Document;
//import org.lemurproject.galago.core.parse.Document.DocumentComponents;
//import org.lemurproject.galago.core.parse.stem.KrovetzStemmer;
//import org.lemurproject.galago.core.parse.stem.Stemmer;
//import org.lemurproject.galago.core.retrieval.GroupRetrieval;
//import org.lemurproject.galago.core.retrieval.Results;
//import org.lemurproject.galago.core.retrieval.Retrieval;
//import org.lemurproject.galago.core.retrieval.ScoredDocument;
//import org.lemurproject.galago.core.retrieval.prf.WeightedTerm;
//import org.lemurproject.galago.core.retrieval.query.AnnotatedNode;
//import org.lemurproject.galago.core.retrieval.query.Node;
//import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
//import org.lemurproject.galago.core.util.WordLists;
//import org.lemurproject.galago.utility.MathUtils;
//import org.lemurproject.galago.utility.Parameters;
//import org.lemurproject.galago.utility.lists.Scored;
//
//
//import java.io.BufferedWriter;
//import java.util.*;
//import java.util.concurrent.Semaphore;
//import java.util.logging.Logger;
//
//import java.util.List;
//import java.util.Set;
//import java.util.logging.Logger;
//
//public class EmbeddingBasedRM implements ExpansionModel {
//
//    private static final Logger logger = Logger.getLogger("Embedding based relevance model");
//    private RelevanceModel1 rm1;
//    private double Beta; // to interpolate term_matching and semantic_similarity ditribution
//    private double defaultFbOrigWeight;// to interpolate fb language model and original language model = alpha
//
//    private final Retrieval retrieval;
//    private WordEmbedding we = null;
//    private List<ScoredDocument> feedbackDocs = null;
//
//
//    public EmbeddingBasedRM(Retrieval r, WordEmbedding we) throws Exception {
//
//        this.retrieval = r;
//        this.rm1 = new RelevanceModel1(r);
//        this.we = we ;
//        defaultFbOrigWeight = r.getGlobalParameters().get("fbOrigWeight", 0.75);
//        Beta = 0.75;  /// should retrieve from r ****************
//    }
//
//    @Override
//    public Node expand(Node root, Parameters queryParameters) throws Exception {
//
//        double fbOrigWeight = queryParameters.get("fbOrigWeight", defaultFbOrigWeight);
//        if (fbOrigWeight == 1.0) {
//            logger.info("fbOrigWeight is invalid (1.0)");
//            return root;
//        }
//        int fbDocs = (int) Math.round(root.getNodeParameters().get("fbDocs", queryParameters.get("fbDocs", (double) rm1.getDefaultFbDocs())));
//        int fbTerms = (int) Math.round(root.getNodeParameters().get("fbTerm", queryParameters.get("fbTerm", (double) rm1.getDefaultFbTerms())));
//
//        if (fbDocs <= 0 || fbTerms <= 0) {
//            logger.info("fbDocs, or fbTerms is invalid, no expansion possible. (<= 0)");
//            return root;
//        }
//
//        // transform query to ensure it will run
//        Parameters fbParams = Parameters.create();
//        fbParams.set("requested", fbDocs);
//        // first pass is asserted to be document level
//        fbParams.set("passageQuery", false);
//        fbParams.set("extentQuery", false);
//        fbParams.setBackoff(queryParameters);// add text and number of the query
//
//        Node termMatching = rm1.expand(root, queryParameters); // term matching
//        if (termMatching == root) {
//            // no expansion performed
//            return root;
//        }
//
//
//        // get some initial results
//        feedbackDocs = rm1.getFeedbackDocs(root, fbParams);
//        if (feedbackDocs.isEmpty()) {
//            System.err.println("there in no feedback documents");
//            return root;
//        }
//
//        // extract grams from results
//        Node transformed = retrieval.transformQuery(root.clone(), fbParams);
//        Set<String> stemmedQueryTerms = rm1.stemTerms(rm1.getStemmer(), StructuredQuery.findQueryTerms(transformed));
//        Set<String> exclusions = (fbParams.isString("rmstopwords")) ? WordLists.getWordList(fbParams.getString("rmstopwords")) : rm1.getExclusionTerms();
//        Set<String> inclusions = null;
//        if (fbParams.isString("rmwhitelist")) {
//            inclusions = WordLists.getWordList(fbParams.getString("rmwhitelist"));
//        } else {
//            inclusions = rm1.getInclusionTerms();
//        }
//
//        List<WeightedTerm> weightedTerms = extractGrams( retrieval, feedbackDocs, rm1.getStemmer(), fbParams, stemmedQueryTerms, exclusions, inclusions, fbDocs);
//
//        // select some terms to form exp query node
//        Node semanticSim = RelevanceModel1.generateExpansionQuery(weightedTerms, fbTerms);
//
//        Node feedbackModel = new Node("combine");
//        feedbackModel.addChild(termMatching);
//        feedbackModel.addChild(semanticSim);
//        feedbackModel.getNodeParameters().set("0", Beta);
//        feedbackModel.getNodeParameters().set("1", 1.0 - Beta);
//
//        Node expNode = new Node("combine");
//        expNode.addChild(root);
//        expNode.addChild(feedbackModel);
//        expNode.getNodeParameters().set("0", fbOrigWeight);
//        expNode.getNodeParameters().set("1", 1.0 - fbOrigWeight);
//
//
//        return expNode;
//    }
//
//    @Override
//    public Node expand(Node root, Parameters queryParameters, BufferedWriter outfile) throws Exception {
//        return null;
//    }
//
//    public  List<WeightedTerm> extractGrams( Retrieval retrieval, List<ScoredDocument> initialResults, Stemmer stemmer, Parameters fbParams, Set<String> queryTerms, Set<String> exclusionTerms, Set<String> inclusionTerms, int fbDocs) throws Exception {
//        // get term frequencies in documents
//        Map<ScoredDocument, Map<String, Integer>> DocTermCount = countGrams(retrieval, initialResults, stemmer,fbParams, exclusionTerms, inclusionTerms);
//        Map<String, Map<ScoredDocument, Integer>> counts = RelevanceModel1.countGrams(retrieval, initialResults, stemmer, fbParams, queryTerms,exclusionTerms,inclusionTerms );
//
//        // compute term weights
//        List<WeightedTerm> scored = scoreGrams(queryTerms, DocTermCount, counts, fbDocs);
//
//        // sort by weight
//        Collections.sort(scored);
//
//        return scored;
//    }
//
//    public  Map<ScoredDocument, Map<String, Integer>> countGrams(Retrieval retrieval, List<ScoredDocument> results,Stemmer stemmer,Parameters fbParams,Set<String> exclusionTerms, Set<String> inclusionTerms) throws Exception{
//        Map<ScoredDocument, Map<String, Integer>> counts = new HashMap<ScoredDocument, Map<String, Integer>>();
//        Document doc;
//
//        DocumentComponents corpusParams = new DocumentComponents(true, false, true);
//
//        String group = fbParams.get("group", (String) null);
//
//        for (ScoredDocument sd : results) {
//            if (group != null && retrieval instanceof GroupRetrieval) {
//                doc = ((GroupRetrieval) retrieval).getDocument(sd.documentName, corpusParams, group);
//            } else {
//                doc = retrieval.getDocument(sd.documentName, corpusParams);
//            }
//
//            if (doc == null) {
//                logger.info("Failed to retrieve document: " + sd.documentName + " -- RM skipping document.");
//                continue;
//            }
//            counts.put(sd, new HashMap<String, Integer>());
//            List<String> docterms = doc.terms;
//
//            sd.annotation = new AnnotatedNode();
//            sd.annotation.extraInfo = "" + docterms.size();
//
//            for (String term : docterms) {
//                // perform stopword and query term filtering here //
//                if (inclusionTerms != null && !inclusionTerms.contains(term)) {
//                    continue; // not on the whitelist
//                }
//                if (exclusionTerms.contains(term)) {
//                    continue; // on the blacklist
//                }
//                if (!counts.get(sd).containsKey(term)) {
//                    counts.get(sd).put(term, 1);
//                }
//                else {
//                    counts.get(sd).put(term, counts.get(sd).get(term)+1);
//                }
//            }
//        }
//        return counts;
//    }
//
//    public List<WeightedTerm> scoreGrams(Set<String> queryTerms, Map<ScoredDocument, Map<String, Integer>> DocTermCount, Map<String, Map<ScoredDocument, Integer>> counts, int fbDocs) throws Exception {
//        List<WeightedTerm> grams = new ArrayList<WeightedTerm>();
//        Map<ScoredDocument, Integer> termCounts;
//
//        for (String term : counts.keySet()) {
//            if (we.getVector(term) == null)
//                continue;
//            RelevanceModel1.WeightedUnigram g = new RelevanceModel1.WeightedUnigram(term);
//            termCounts = counts.get(term);
//            for (ScoredDocument sd : termCounts.keySet()) {
//                // we forced this into the scored document earlier
//                int length = Integer.parseInt(sd.annotation.extraInfo);
//                g.score += semanticSimilarity(sd, term, DocTermCount.get(sd), queryTerms) * termCounts.get(sd) / length;
//            }
//            // 1 / fbDocs from the RelevanceModel source code
//            g.score *= (1.0 / fbDocs);
//            grams.add(g);
//        }
//
//        return grams;
//    }
//    public Double semanticSimilarity(ScoredDocument sd, String term, Map<String, Integer> DocTermcount, Set<String> queryTerms) throws Exception{
//
//        // calc denominator
//        Double allSimilarity = getAllSimilarity(we, term, DocTermcount);
//        if (allSimilarity == 0.0) {
//            System.err.println("all similarity is zero!!");
//        }
//        Double mult = 1.0;
//        for (String queryTerm : queryTerms){
//            if (!DocTermcount.keySet().contains(queryTerm))
//                return 0.0 ;
//            mult *= we.getSimilarity(queryTerm, term)*DocTermcount.get(queryTerm) / allSimilarity;
//        }
//        return mult;
//    }
//    public Double getAllSimilarity(WordEmbedding we,String term, Map<String, Integer>  DocTermcount) throws Exception{
//        Double sum = 0.0;
//        for (String docTerm : DocTermcount.keySet()){
//            sum += we.getSimilarity(term, docTerm)*DocTermcount.get(docTerm);
//        }
//        return sum;
//    }
//}
